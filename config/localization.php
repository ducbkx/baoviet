<?php

return [

    'supported-locales' => ['vi', 'en'],

    'accept-language-header' => true,

    'hide-default-in-url' => false,

    'redirection-code' => 302,

    'utf-8-suffix' => '.UTF-8',

    'facade' => 'Localization',

    'ignored-uri' => [
        '/api/*',
        '/dashboard',
        '/dashboard/*',
    ],

    'locales' => [
        'vi' => [
            'name'     => 'Vietnamese',
            'script'   => 'Latn',
            'dir'      => 'ltr',
            'native'   => 'Tiếng Việt',
            'regional' => 'vi_VN',
        ],

        'en' => [
            'name'     => 'English',
            'script'   => 'Latn',
            'dir'      => 'ltr',
            'native'   => 'English',
            'regional' => 'en_GB',
        ],
    ],

];
