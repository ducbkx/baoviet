const mix = require('laravel-mix');

/**
 * The externals library.
 *
 * @type {object}
 */
const externals = {
    'react': 'React',
    'react-dom': 'ReactDOM',
    'jquery': 'jQuery',
};

mix.disableSuccessNotifications()
    .js('resources/js/main.js', 'public/js')
    .sass('resources/sass/main.scss', 'public/css');

mix.styles([
    'resources/libs/jquery-ui/jquery-ui.min.css',
    'resources/libs/swiper4.7/swiper.min.css',
    'resources/libs/slickjs/slick.css',
    'resources/libs/select2/select2.min.css',
    'resources/libs/magnific-popup/magnific-popup.min.css',
], 'public/css/vendor.css');

mix.scripts([
    'resources/libs/_jquery/jquery.min.js',
    'resources/libs/jquery-ui/jquery-ui.min.js',
    'resources/libs/swiper4.7/swiper.jquery.min.js',
    'resources/libs/slickjs/slick.js',
    'resources/libs/select2/select2.js',
    'resources/libs/headroom/headroom.js',
    'resources/libs/imagesloaded/imagesloaded.pkgd.js',
    'resources/libs/jquery.matchHeight/jquery.matchHeight.min.js',
    'resources/libs/jquery.waypoints/jquery.waypoints.min.js',
    'resources/libs/jquery.countTo/jquery.countTo.min.js',
    'resources/libs/jquery-one-page/jquery.nav.js',
    'resources/libs/mdGridJs/mdGridJs.js',
    'resources/libs/accordion/awe-accordion.min.js',
    'resources/libs/tabs/awe-tabs.min.js',
    'resources/libs/magnific-popup/jquery.magnific-popup.min.js',
], 'public/js/vendor.js');

if (mix.inProduction()) {
    mix.version();
}

mix.webpackConfig({
    externals
});

mix.options({
    processCssUrls: false
});
