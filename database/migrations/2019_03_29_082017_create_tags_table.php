<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
        });

        Schema::create('tags_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('tag_id')->index();
            $table->string('locale')->index();

            $table->string('name');
            $table->string('slug')->unique();

            $table->unique(['tag_id', 'locale']);
            $table->foreign('tag_id')
                  ->references('id')->on('tags')->onDelete('cascade');
        });

        Schema::create('taggables', function (Blueprint $table) {
            $table->bigIncrements('tag_id');
            $table->unsignedBigInteger('taggable_id');
            $table->string('taggable_type')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tags_translations');
        Schema::dropIfExists('taggables');
        Schema::dropIfExists('tags');
    }
}
