<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('author_id')->default(0)->index();

            $table->string('status', 20)->default('pending')->index(); // ['pending', 'future', 'publish']
            $table->integer('order')->default(0);
            $table->datetime('published_date')->nullable();
            $table->timestamps();

            $table->foreign('author_id')
                  ->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('news_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('news_id')->index();
            $table->string('locale')->index();

            $table->string('title');
            $table->string('slug')->unique();
            $table->text('excerpt');
            $table->longText('content');
            $table->longText('content_filtered')->nullable();

            $table->unique(['news_id', 'locale']);
            $table->foreign('news_id')
                  ->references('id')->on('news')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_translations');
        Schema::dropIfExists('news');
    }
}
