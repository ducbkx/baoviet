<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type')->index(); // The class name of the page type.
            $table->string('status', 20)->index()->default('pending'); // ['pending', 'publish']
            $table->integer('order')->default(0);
            $table->timestamps();
        });

        Schema::create('pages_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('page_id')->index();
            $table->string('locale')->index();

            $table->string('title');
            $table->string('slug')->unique();
            $table->longText('content');
            $table->longText('content_filtered')->nullable();

            $table->unique(['page_id', 'locale']);
            $table->foreign('page_id')
                  ->references('id')->on('pages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages_translations');
        Schema::dropIfExists('pages');
    }
}
