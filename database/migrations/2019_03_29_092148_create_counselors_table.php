<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCounselorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('counselors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('gender', ['women', 'men', 'another'])->default('women')->index();
            $table->smallInteger('year_of_birth')->nullable();
            $table->string('job_title')->nullable();
            $table->integer('rate_value')->default(0);
            $table->string('avatar')->nullable(); // The image of relative URL.
            $table->timestamps();
        });

        Schema::create('counselors_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('counselor_id')->index();
            $table->string('locale')->index();

            $table->string('first_name');
            $table->string('last_name');
            $table->string('display_name');
            $table->text('private_infomation')->nullable();
            $table->text('public_infomation')->nullable();

            $table->index(['first_name', 'last_name']);
            $table->unique(['counselor_id', 'locale']);
            $table->foreign('counselor_id')
                  ->references('id')->on('counselors')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('counselors_translations');
        Schema::dropIfExists('counselors');
    }
}
