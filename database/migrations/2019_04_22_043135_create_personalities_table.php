<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonalitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personalities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
        });

        Schema::create('personalities_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('personality_id')->index();
            $table->string('locale')->index();

            $table->string('name');
            $table->string('slug')->unique();
            $table->text('description');

            $table->unique(['personality_id', 'locale']);
            $table->foreign('personality_id')
                ->references('id')->on('personalities')->onDelete('cascade');
        });

        Schema::create('counselor_personality', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('counselor_id');
            $table->foreign('counselor_id')
                ->references('id')->on('counselors')->onDelete('cascade');
            $table->unsignedBigInteger('personality_id');
            $table->foreign('personality_id')
                ->references('id')->on('personalities')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personalities_translations');
        Schema::dropIfExists('personalities');
        Schema::dropIfExists('counselor_personality');
    }
}
