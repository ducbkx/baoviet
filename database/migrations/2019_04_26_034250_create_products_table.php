<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('image')->nullable();
            $table->unsignedBigInteger('author_id')->default(0)->index();

            $table->string('status', 20)->default('pending')->index(); // ['pending', 'future', 'publish']
            $table->integer('order')->default(0);
            $table->boolean('is_future')->default(0);
            $table->datetime('published_date')->nullable();
            $table->timestamps();

            $table->foreign('author_id')
                ->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('products_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('product_id')->index();
            $table->string('locale')->index();

            $table->string('title');
            $table->string('slug')->unique();
            $table->text('excerpt');
            $table->longText('content');
            $table->longText('content_filtered')->nullable();

            $table->unique(['product_id', 'locale']);
            $table->foreign('product_id')
                ->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
        Schema::dropIfExists('products_translations');
    }
}
