<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('status', 20)->index()->default('pending'); // ['pending', 'publish']
            $table->integer('order')->default(0);
            $table->timestamps();
        });

        Schema::create('services_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('service_id')->index();
            $table->string('locale')->index();

            $table->string('title');
            $table->string('slug')->unique();
            $table->longText('content');
            $table->longText('content_filtered')->nullable();

            $table->unique(['service_id', 'locale']);
            $table->foreign('service_id')
                  ->references('id')->on('services')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services_translations');
        Schema::dropIfExists('services');
    }
}
