<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
        });

        Schema::create('categories_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('category_id')->index();
            $table->string('locale')->index();

            $table->string('name');
            $table->string('slug')->unique();
            $table->text('description');

            $table->unique(['category_id', 'locale']);
            $table->foreign('category_id')
                  ->references('id')->on('categories')->onDelete('cascade');
        });

        Schema::create('categoriables', function (Blueprint $table) {
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('categoriable_id');
            $table->string('categoriable_type')->index();
            $table->integer('order')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories_translations');
        Schema::dropIfExists('categoriables');
        Schema::dropIfExists('categories');
    }
}
