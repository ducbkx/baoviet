(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["/js/gutenberg"],{

/***/ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/src/index.js?!./resources/js/pages/editor.css":
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/postcss-loader/src??ref--6-2!./resources/js/pages/editor.css ***!
  \***********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\r\n.editor-nav {\r\n  position: absolute;\r\n  display: flex;\r\n  justify-content: center;\r\n  align-items: center;\r\n  width: 100%;\r\n  height: 56px;\r\n}\r\n\r\n.editor-nav * {\r\n  z-index: 99;\r\n}\r\n\r\n@media (min-width: 782px) {\r\n  .edit-post-layout__content {\r\n    margin-left: 0 !important;\r\n  }\r\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./resources/js/globals/api-fetch.js":
/*!*******************************************!*\
  !*** ./resources/js/globals/api-fetch.js ***!
  \*******************************************/
/*! exports provided: getPage, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getPage", function() { return getPage; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _fake_data_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./fake-data.js */ "./resources/js/globals/fake-data.js");
/* harmony import */ var _fake_media_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./fake-media.js */ "./resources/js/globals/fake-media.js");


function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/* eslint no-cond-assign: off */


function getPage() {
  var type = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'page';
  return JSON.parse(localStorage.getItem('g-editor-page')) || _fake_data_js__WEBPACK_IMPORTED_MODULE_1__["pages"][type];
}

function savePage(data) {
  var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'page';

  var item = _objectSpread({}, getPage(type), data, {
    content: {
      raw: data.content,
      rendered: data.content.replace(/(<!--.*?-->)/g, '')
    }
  });

  localStorage.setItem('g-editor-page', JSON.stringify(item));
}

function route(pattern, pathname) {
  var res = {};
  var r = pattern.split('/'),
      l = r.length,
      p = pathname.split('/');
  var i = 0;

  for (; i < l; i++) {
    if (r[i] === p[i]) {
      continue;
    }

    if (r[i].charAt(0) === '{' && r[i].charAt(r[i].length - 1) === '}' && p[i]) {
      res[r[i].substring(1, r[i].length - 1)] = p[i];
      continue;
    }

    return false;
  }

  if (p[i]) {
    return false;
  }

  return res;
}

var apiFetch =
/*#__PURE__*/
function () {
  var _ref = _asyncToGenerator(
  /*#__PURE__*/
  _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(options) {
    var res, rt, method, path, data, _path$split, _path$split2, _path, file;

    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            console.log(options.path, options);
            res = {};
            method = options.method, path = options.path, data = options.data;
            _path$split = path.split('?'), _path$split2 = _slicedToArray(_path$split, 1), _path = _path$split2[0]; // Types

            if (!route('/wp/v2/types', _path)) {
              _context.next = 8;
              break;
            }

            res = _fake_data_js__WEBPACK_IMPORTED_MODULE_1__["types"];
            _context.next = 51;
            break;

          case 8:
            if (!(rt = route('/wp/v2/types/{type}', _path))) {
              _context.next = 12;
              break;
            }

            res = _fake_data_js__WEBPACK_IMPORTED_MODULE_1__["types"][rt.type];
            _context.next = 51;
            break;

          case 12:
            if (!route('/wp/v2/pages', _path)) {
              _context.next = 16;
              break;
            }

            res = [getPage()];
            _context.next = 51;
            break;

          case 16:
            if (!(route('/wp/v2/pages/{id}', _path) || route('/wp/v2/pages/{id}/autosaves', _path))) {
              _context.next = 21;
              break;
            }

            if ((method === 'POST' || method === 'PUT') && data) {
              savePage(options.data);
            }

            res = getPage();
            _context.next = 51;
            break;

          case 21:
            if (!(rt = route('/wp/v2/posts', _path))) {
              _context.next = 25;
              break;
            }

            res = [getPage('post')];
            _context.next = 51;
            break;

          case 25:
            if (!(route('/wp/v2/posts/{id}', _path) || route('/wp/v2/posts/{id}/autosaves', _path))) {
              _context.next = 30;
              break;
            }

            if ((method === 'POST' || method === 'PUT') && data) {
              savePage(options.data, 'post');
            }

            res = getPage('post');
            _context.next = 51;
            break;

          case 30:
            if (!route('/wp/v2/media', _path)) {
              _context.next = 50;
              break;
            }

            if (!(method === 'OPTIONS')) {
              _context.next = 35;
              break;
            }

            res = {
              headers: {
                get: function get(value) {
                  if (value === 'allow') {
                    return ['POST'];
                  }
                }
              }
            };
            _context.next = 48;
            break;

          case 35:
            if (!(method === 'POST')) {
              _context.next = 47;
              break;
            }

            file = options.body.get('file');

            if (!file) {
              _context.next = 43;
              break;
            }

            _context.next = 40;
            return Object(_fake_media_js__WEBPACK_IMPORTED_MODULE_2__["createMedia"])(file);

          case 40:
            _context.t0 = _context.sent;
            _context.next = 44;
            break;

          case 43:
            _context.t0 = {};

          case 44:
            res = _context.t0;
            _context.next = 48;
            break;

          case 47:
            res = _fake_media_js__WEBPACK_IMPORTED_MODULE_2__["mediaList"];

          case 48:
            _context.next = 51;
            break;

          case 50:
            if (rt = route('/wp/v2/media/{id}', _path)) {
              res = _fake_media_js__WEBPACK_IMPORTED_MODULE_2__["mediaList"][+rt.id - 1];
            } // Themes
            else if (route('/wp/v2/themes', _path)) {
                res = _fake_data_js__WEBPACK_IMPORTED_MODULE_1__["themes"];
              } // Taxonomies
              else if (route('/wp/v2/taxonomies', _path)) {
                  res = _fake_data_js__WEBPACK_IMPORTED_MODULE_1__["taxonomies"];
                } else if (rt = route('/wp/v2/taxonomies/{name}', _path)) {
                  res = _fake_data_js__WEBPACK_IMPORTED_MODULE_1__["taxonomies"][rt.name];
                } // Categories
                else if (route('/wp/v2/categories', _path)) {
                    res = _fake_data_js__WEBPACK_IMPORTED_MODULE_1__["categories"];
                  } // Users
                  else if (route('/wp/v2/users', _path)) {
                      res = _fake_data_js__WEBPACK_IMPORTED_MODULE_1__["users"];
                    } else {
                      console.warn('Unmatched route:', method || 'GET', path, data);
                    }

          case 51:
            console.log(res);
            return _context.abrupt("return", res);

          case 53:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function apiFetch(_x) {
    return _ref.apply(this, arguments);
  };
}();

/* harmony default export */ __webpack_exports__["default"] = (apiFetch);

/***/ }),

/***/ "./resources/js/globals/fake-data.js":
/*!*******************************************!*\
  !*** ./resources/js/globals/fake-data.js ***!
  \*******************************************/
/*! exports provided: types, pages, themes, taxonomies, categories, users */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "types", function() { return types; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pages", function() { return pages; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "themes", function() { return themes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "taxonomies", function() { return taxonomies; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "categories", function() { return categories; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "users", function() { return users; });
var date = new Date().toISOString(); // Post types and Pages

var types = {
  post: {
    id: 1,
    name: 'Posts',
    rest_base: 'posts',
    slug: 'post',
    supports: {
      author: false,
      comments: false,
      // hide discussion-panel
      'custom-fields': true,
      editor: true,
      excerpt: false,
      'page-attributes': false,
      // hide page-attributes panel
      revisions: false,
      thumbnail: false,
      // show featured-image panel
      title: true // show title on editor

    },
    taxonomies: ['category', 'post_tag'],
    viewable: true
  },
  page: {
    id: 2,
    name: 'Pages',
    rest_base: 'pages',
    slug: 'page',
    supports: {
      author: false,
      comments: false,
      'custom-fields': false,
      discussion: false,
      editor: true,
      excerpt: false,
      'page-attributes': false,
      revisions: false,
      thumbnail: false,
      title: false
    },
    taxonomies: [],
    viewable: true
  }
};
var pages = {
  page: {
    id: 1,
    content: {
      raw: '',
      rendered: ''
    },
    date: date,
    date_gmt: date,
    title: {
      raw: 'Preview page',
      rendered: 'Preview page'
    },
    excerpt: {
      raw: '',
      rendered: ''
    },
    status: 'draft',
    revisions: {
      count: 0,
      last_id: 0
    },
    parent: 0,
    theme_style: true,
    type: 'page',
    link: "".concat(window.location.origin, "/preview"),
    categories: [],
    featured_media: 0,
    permalink_template: "".concat(window.location.origin, "/preview"),
    preview_link: "".concat(window.location.origin, "/preview"),
    _links: {
      'wp:action-assign-categories': [],
      'wp:action-create-categories': []
    }
  },
  post: {
    id: 1,
    content: {
      raw: '',
      rendered: ''
    },
    date: date,
    date_gmt: date,
    title: {
      raw: 'Preview post',
      rendered: 'Preview post'
    },
    excerpt: {
      raw: '',
      rendered: ''
    },
    status: 'draft',
    revisions: {
      count: 0,
      last_id: 0
    },
    parent: 0,
    theme_style: true,
    type: 'post',
    link: "".concat(window.location.origin, "/preview"),
    categories: [],
    featured_media: 0,
    permalink_template: "".concat(window.location.origin, "/preview"),
    preview_link: "".concat(window.location.origin, "/preview"),
    _links: {
      'wp:action-assign-categories': [],
      'wp:action-create-categories': []
    }
  }
}; // Themes

var themes = [{
  theme_supports: {
    formats: ['standard', 'aside', 'image', 'video', 'quote', 'link', 'gallery', 'audio'],
    'post-thumbanials': true,
    'responsive-embeds': false
  }
}]; // Taxonomies and Categories

var taxonomies = {
  category: {
    name: 'Categories',
    slug: 'category',
    description: '',
    types: ['post'],
    hierarchical: true,
    rest_base: 'categories',
    _links: {
      collection: [],
      'wp:items': []
    },
    visibility: {
      show_ui: true
    }
  },
  post_tag: {
    name: 'Tags',
    slug: 'post_tag',
    description: '',
    types: ['post'],
    hierarchical: false,
    rest_base: 'tags',
    _links: {
      collection: [],
      'wp:items': []
    },
    visibility: {
      show_ui: true
    }
  }
};
var categories = [{
  id: 2,
  count: 3,
  description: 'Neque quibusdam nihil sequi quia et inventore dolorem dolores...',
  link: 'https://demo.wp-api.org/category/aut-architecto-nihil/',
  name: 'Aut architecto nihil',
  slug: 'aut-architecto-nihil',
  taxonomy: 'category',
  parent: 0,
  meta: [],
  _links: {
    self: [],
    collection: [],
    about: [],
    'wp:post_type': []
  }
}, {
  id: 11,
  count: 7,
  description: 'Rem recusandae velit et incidunt labore qui explicabo veritatis...',
  link: 'https://demo.wp-api.org/category/facilis-dignissimos/',
  name: 'Facilis dignissimos',
  slug: 'facilis-dignissimos',
  taxonomy: 'category',
  parent: 0,
  meta: [],
  _links: {
    self: [],
    collection: [],
    about: [],
    'wp:post_type': []
  }
}, {
  id: 1,
  count: 5,
  description: '',
  link: 'https://demo.wp-api.org/category/uncategorized/',
  name: 'Uncategorized',
  slug: 'uncategorized',
  taxonomy: 'category',
  parent: 0,
  meta: [],
  _links: {
    self: [],
    collection: [],
    about: [],
    'wp:post_type': []
  }
}]; // Users

var users = [{
  id: 1,
  name: 'Human Made',
  url: '',
  description: '',
  link: 'https://demo.wp-api.org/author/humanmade/',
  slug: 'humanmade',
  avatar_urls: {
    24: 'http://2.gravatar.com/avatar/83888eb8aea456e4322577f96b4dbaab?s=24&d=mm&r=g',
    48: 'http://2.gravatar.com/avatar/83888eb8aea456e4322577f96b4dbaab?s=48&d=mm&r=g',
    96: 'http://2.gravatar.com/avatar/83888eb8aea456e4322577f96b4dbaab?s=96&d=mm&r=g'
  },
  meta: [],
  _links: {
    self: [],
    collection: []
  }
}];

/***/ }),

/***/ "./resources/js/globals/fake-media.js":
/*!********************************************!*\
  !*** ./resources/js/globals/fake-media.js ***!
  \********************************************/
/*! exports provided: mediaList, getMedia, createMedia */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mediaList", function() { return mediaList; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getMedia", function() { return getMedia; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createMedia", function() { return createMedia; });
var date = new Date().toISOString();
var origin = window.location.origin; // List of images

var mediaList = [];
function getMedia(id) {
  var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var sizes = {};

  if (params.thumbnail) {
    sizes.thumbnail = {
      source_url: params.thumbnail
    };
  }

  return {
    id: id,
    title: {
      raw: '',
      rendered: ''
    },
    caption: {
      raw: '',
      rendered: ''
    },
    date_gmt: date,
    date: date,
    media_type: params.media_type,
    mime_type: params.mime_type,
    source_url: params.source_url,
    // link: params.source_url,
    media_details: {
      file: '',
      width: 0,
      height: 0,
      image_meta: {},
      sizes: sizes
    }
  };
}
function createMedia(file) {
  return new Promise(function (resolve) {
    var reader = new window.FileReader();

    reader.onload = function () {
      // Create media and add to list
      var img = getMedia(mediaList.length + 1, {
        media_type: file.type.split('/')[0],
        mime_type: file.type,
        source_url: reader.result
      });
      mediaList.push(img);
      resolve(img);
    };

    reader.readAsDataURL(file);
  });
} // Load media (images)

mediaList.push(getMedia(1, {
  media_type: 'image',
  mime_type: 'image/jpeg',
  source_url: "".concat(origin, "/img1.jpg")
}));
mediaList.push(getMedia(2, {
  media_type: 'image',
  mime_type: 'image/jpeg',
  source_url: "".concat(origin, "/img2.jpeg")
}));
mediaList.push(getMedia(3, {
  media_type: 'image',
  mime_type: 'image/png',
  source_url: "".concat(origin, "/img3.png")
})); // Load media (videos)

mediaList.push(getMedia(4, {
  media_type: 'video',
  mime_type: 'video/mp4',
  source_url: "".concat(origin, "/video1.mp4"),
  thumbnail: "".concat(origin, "/video1-thumb.jpg")
}));
mediaList.push(getMedia(5, {
  media_type: 'video',
  mime_type: 'video/mp4',
  source_url: "".concat(origin, "/video2.mp4"),
  thumbnail: "".concat(origin, "/video2-thumb.jpg")
})); // Load media (audios)

mediaList.push(getMedia(6, {
  media_type: 'audio',
  mime_type: 'audio/mp3',
  source_url: "".concat(origin, "/audio1.mp3"),
  thumbnail: "".concat(origin, "/audio1-thumb.png")
}));

/***/ }),

/***/ "./resources/js/globals/index.js":
/*!***************************************!*\
  !*** ./resources/js/globals/index.js ***!
  \***************************************/
/*! exports provided: addQueryArgs */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addQueryArgs", function() { return addQueryArgs; });
/* harmony import */ var _api_fetch__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./api-fetch */ "./resources/js/globals/api-fetch.js");
/* harmony import */ var qs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! qs */ "./node_modules/qs/lib/index.js");
/* harmony import */ var qs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(qs__WEBPACK_IMPORTED_MODULE_1__);
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// Internal Dependencies


/**
 * Appends arguments to the query string of the url
 *
 * @param  {String} url   URL
 * @param  {Object} args  Query Args
 *
 * @return {String}       Updated URL
 */

function addQueryArgs(url, args) {
  var queryStringIndex = url.indexOf('?');
  var query = queryStringIndex !== -1 ? Object(qs__WEBPACK_IMPORTED_MODULE_1__["parse"])(url.substr(queryStringIndex + 1)) : {};
  var baseUrl = queryStringIndex !== -1 ? url.substr(0, queryStringIndex) : url;
  return baseUrl + '?' + Object(qs__WEBPACK_IMPORTED_MODULE_1__["stringify"])(_objectSpread({}, query, args));
}
window.wp = {
  apiFetch: _api_fetch__WEBPACK_IMPORTED_MODULE_0__["default"],
  url: {
    addQueryArgs: addQueryArgs
  }
};

/***/ }),

/***/ "./resources/js/gutenberg.js":
/*!***********************************!*\
  !*** ./resources/js/gutenberg.js ***!
  \***********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_polyfill__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/polyfill */ "./node_modules/@babel/polyfill/lib/index.js");
/* harmony import */ var _babel_polyfill__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_polyfill__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _globals_index__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./globals/index */ "./resources/js/globals/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-dom */ "react-dom");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _pages_editor__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pages/editor */ "./resources/js/pages/editor.js");




react_dom__WEBPACK_IMPORTED_MODULE_2___default.a.render(React.createElement(_pages_editor__WEBPACK_IMPORTED_MODULE_3__["default"], null), document.getElementById('root'));

/***/ }),

/***/ "./resources/js/pages/editor.css":
/*!***************************************!*\
  !*** ./resources/js/pages/editor.css ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader??ref--6-1!../../../node_modules/postcss-loader/src??ref--6-2!./editor.css */ "./node_modules/css-loader/index.js?!./node_modules/postcss-loader/src/index.js?!./resources/js/pages/editor.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./resources/js/pages/editor.js":
/*!**************************************!*\
  !*** ./resources/js/pages/editor.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _frontkom_gutenberg_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @frontkom/gutenberg-js */ "./node_modules/@frontkom/gutenberg-js/build/js/gutenberg-js.js");
/* harmony import */ var _frontkom_gutenberg_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_frontkom_gutenberg_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _frontkom_gutenberg_js_build_css_block_library_style_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @frontkom/gutenberg-js/build/css/block-library/style.css */ "./node_modules/@frontkom/gutenberg-js/build/css/block-library/style.css");
/* harmony import */ var _frontkom_gutenberg_js_build_css_block_library_style_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_frontkom_gutenberg_js_build_css_block_library_style_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _frontkom_gutenberg_js_build_css_style_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @frontkom/gutenberg-js/build/css/style.css */ "./node_modules/@frontkom/gutenberg-js/build/css/style.css");
/* harmony import */ var _frontkom_gutenberg_js_build_css_style_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_frontkom_gutenberg_js_build_css_style_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _editor_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./editor.css */ "./resources/js/pages/editor.css");
/* harmony import */ var _editor_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_editor_css__WEBPACK_IMPORTED_MODULE_4__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }


 // Gutenberg JS Style





var Editor =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Editor, _React$Component);

  function Editor() {
    _classCallCheck(this, Editor);

    return _possibleConstructorReturn(this, _getPrototypeOf(Editor).apply(this, arguments));
  }

  _createClass(Editor, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var settings = {
        alignWide: true,
        availableTemplates: [],
        allowedBlockTypes: true,
        disableCustomColors: false,
        disablePostFormats: false,
        titlePlaceholder: 'Add title',
        bodyPlaceholder: 'Insert your custom block',
        isRTL: false,
        autosaveInterval: 0,
        postLock: {
          isLocked: false
        },
        canPublish: false,
        canSave: true,
        canAutosave: true,
        mediaLibrary: true
      }; // Disable tips
      // data.dispatch('core/nux').disableTips();
      // Initialize the editor

      window._wpLoadGutenbergEditor = new Promise(function (resolve) {
        Object(_frontkom_gutenberg_js__WEBPACK_IMPORTED_MODULE_1__["domReady"])(function () {
          resolve(_frontkom_gutenberg_js__WEBPACK_IMPORTED_MODULE_1__["editPost"].initializeEditor('editor', 'page', 1, settings, {}));
        });
      });
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        id: "editor",
        className: "gutenberg__editor"
      }));
    }
  }]);

  return Editor;
}(react__WEBPACK_IMPORTED_MODULE_0___default.a.Component);

/* harmony default export */ __webpack_exports__["default"] = (Editor);

/***/ }),

/***/ "./resources/sass/app.scss":
/*!*********************************!*\
  !*** ./resources/sass/app.scss ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!*******************************************************************!*\
  !*** multi ./resources/js/gutenberg.js ./resources/sass/app.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! C:\Users\ducbk\code\baovietnhantho.local\resources\js\gutenberg.js */"./resources/js/gutenberg.js");
module.exports = __webpack_require__(/*! C:\Users\ducbk\code\baovietnhantho.local\resources\sass\app.scss */"./resources/sass/app.scss");


/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "React" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = React;

/***/ }),

/***/ "react-dom":
/*!***************************!*\
  !*** external "ReactDOM" ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ReactDOM;

/***/ })

},[[0,"/js/manifest","/js/vendor"]]]);