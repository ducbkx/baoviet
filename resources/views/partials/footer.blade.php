<div class="container">
    <div class="footer__header">
        <div class="row">
            <div class="col-md-3 col-lg-3 col-xs-offset-0 col-sm-offset-0 col-md-offset-2 col-lg-offset-2 ">
                <div class="footer__backtotop">Lên đầu trang<i class="fa fa-arrow-up"></i></div>
            </div>
            <div class="col-md-7 col-lg-7 ">

                <!-- form-search -->
                <form class="form-search form-search__light footer__search">
                    <input class="form-control" type="text" placeholder="Nhập thông tin cần tìm kiếm"/><span
                        class="form-icon"><i class="fa fa-search"></i></span>
                </form><!-- End / form-search -->

            </div>
        </div>
    </div>

    <div class="footer__content">
        <div class="row">
            <div class="col-lg-5 ">
                <div class="footer__item">
                    <h3 class="footer__itemtitle">Liên hệ với chúng tôi</h3>
                    <div class="row">
                        <div class="col-lg-6 "><a class="footer__info" href="#"><span
                                    class="footer__infoicon"><i class="fa fa-phone"></i></span>
                                <h3 class="footer__infotitle">1900 558899
                                    <small>– nhánh số 4</small>
                                </h3>
                                <p class="footer__infotext">Nếu bạn có những thắc mặc về sản phẩm hoặc dịch vụ,
                                    hãy gọi điện cho chúng tôi theo số hotline để được các tư vấn viên hỗ trợ.
                                    Nội dung cuộc đàm thoại sẽ được ghi âm nhằm mục đích nâng cao chất lượng
                                    dịch vụ.</p></a>
                        </div>
                        <div class="col-lg-5 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-1 ">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-12 "><a class="footer__info"
                                                                                      href="#"><span
                                            class="footer__infoicon"><i class="fa fa-map-marker"></i></span>Chi
                                        nhánh</a>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-12 "><a class="footer__info"
                                                                                      href="#"><span
                                            class="footer__infoicon"><i class="fa fa-envelope"></i></span>Gửi
                                        email</a>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-12 "><a class="footer__info"
                                                                                      href="#"><span
                                            class="footer__infoicon"><i class="fa fa-comments"></i></span>Chat
                                        với tư vấn</a>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-12 "><a class="footer__info"
                                                                                      href="#"><span
                                            class="footer__infoicon"><i class="fa fa-facebook"></i></span>Facebook</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-7 ">
                <div class="row row-eq-height">
                    <div class="col-sm-6 col-md-4 col-lg-4 ">
                        <div class="footer__item footer-item-js">
                            <h3 class="footer__itemtitle">Giải pháp cho bạn</h3>
                            <ul class="footer__list">
                                <li><a href="#">Chăm sóc sức khoẻ</a></li>
                                <li><a href="#">Tương lai cho con trẻ</a></li>
                                <li><a href="#">Tạo dựng tài sản</a></li>
                                <li><a href="#">Vượt qua biến cố bệnh tật</a></li>
                                <li><a href="#">Bảo vệ người trụ cột</a></li>
                                <li><a href="#">Chuẩn bị giai đoạn hưu trí</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-4 ">
                        <div class="footer__item footer-item-js">
                            <h3 class="footer__itemtitle">Giới thiệu</h3>
                            <ul class="footer__list">
                                <li><a href="gioithieu-02.html">Lịch sử phát triển</a></li>
                                <li><a href="gioithieu-03.html">Tầm nhìn sứ mệnh</a></li>
                                <li><a href="gioithieu-04.html">Ban lãnh đạo</a></li>
                                <li><a href="gioithieu-05.html">Giải thưởng và danh hiệu</a></li>
                                <li><a href="gioithieu-06.html">Báo cáo tài chính</a></li>
                                <li><a href="gioithieu.html">Hoạt động cộng đồng</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-4 ">
                        <div class="footer__item footer-item-js">
                            <h3 class="footer__itemtitle">Tuyển dụng</h3>
                            <ul class="footer__list">
                                <li><a href="#">Tuyển dụng cán bộ</a></li>
                                <li><a href="#">Tuyển dụng nhân viên</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-4 ">
                        <div class="footer__item footer-item-js">
                            <h3 class="footer__itemtitle">Dịch vụ khách hàng</h3>
                            <ul class="footer__list">
                                <li><a href="#">Kênh thanh toán</a></li>
                                <li><a href="#">Thay đổi thông tin hợp đồng bảo hiểm</a></li>
                                <li><a href="#">Giải quyết quyền lợi bảo hiểm</a></li>
                                <li><a href="#">Những điều cần biết cho khách hàng</a></li>
                                <li><a href="#">Chăm sóc khách hàng</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__footer">
        <div class="row">
            <div class="col-sm-9 col-md-9 col-lg-9 ">
                <div class="footer__text">
                    <p>Tổng Công ty Bảo Việt Nhân thọ * Trụ sở chính: Tầng 37, Keangnam Ha Noi Landmark Tower,
                        Đường Phạm Hùng, Quận Nam Từ Liêm, Hà Nội </p>
                    <p>Tổng đài Chăm sóc Khách hàng: 1900 558899 nhánh số 4 </p>
                    <p>Ban Quản trị Website: Phòng Marketing, Điện thoại: (+84 24) 6251 7777, Fax: (+84 24) 3577
                        0958 </p>
                    <p>Ghi rõ nguồn "<a href="#">www.baovietnhantho.com.vn</a>" khi sử dụng thông tin từ website
                        này</p>
                </div>
                <p class="footer__copyright">Copyright © 2019 Baoviet. Designed by <a href="beautheme.vn">Beau
                        Agency</a></p>
            </div>
            <div
                class="col-sm-3 col-md-3 col-lg-2 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-1 ">
                <div class="footer__language">
                    <select class="footer__languages">
                        <option value="vietnam" data-img="assets/img/lang/vietnam.png">Tiếng việt</option>
                        <option value="vietnam" data-img="assets/img/lang/vietnam.png">Tiếng việt 2</option>
                        <option value="vietnam" data-img="assets/img/lang/vietnam.png">Tiếng việt 3</option>
                        <option value="vietnam" data-img="assets/img/lang/vietnam.png">Tiếng việt 4</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>
