<div class="header__header">

    <div class="header__logo">
        <a class="logo-dark" href="{{ url('/') }}">
            <img src="{{ asset('img/logo.png') }}" alt="{{ __('Bảo Việt Nhân Thọ') }}"/>
        </a>
    </div>

    <div class="header__fixgrow"></div>

    <ul class="header__info">
        <li class="info-has-children">
            <div class="header__item"><a href="#"><img src="assets/img/svg/header-phone.png" alt=""/>1900 558899
                    <small>– nhánh số 4</small>
                </a></div>
            <div class="info-submenu"><a href="#"><span>1900 558899</span>Nhánh 4 / phím tắt *1166</a><a
                    href="#"><span>1900 558899</span>* Miễn phí với khách hàng đã có hợp đồng với Bảo Việt nhân
                    thọ</a></div>
        </li>
        <li>
            <div class="header__item"><a href="chinhanh.html"><img src="assets/img/svg/header-marker.svg"
                                                                   alt=""/>Chi nhánh</a></div>
        </li>
        <li>
            <div class="header__item header-icon-search"><a href="#"><img src="assets/img/svg/header-search.svg"
                                                                          alt=""/>Tìm kiếm</a></div>
        </li>
    </ul>
</div>

<div class="header__inner">
    <div class="header__desktop">

        <a class="header__iconHome" href="{{ url('/') }}">
            <img src="{{ asset('img/svg/header-home.svg') }}" alt="{{ __('Trang chủ') }}"/>
        </a>

        <div class="header__nav" id="header-nav">

        </div>

        <div class="header__fixgrow"></div>
        <div class="header__user"><img src="assets/img/svg/header-user.svg" alt=""/>MyBV Life</div>
        <div class="header__iconmenu" id="nav-icon2">
            <span></span><span></span><span></span><span></span><span></span><span></span></div>
    </div>
    <div class="header__mobile">
        <div class="header__mobileitem">
            <div class="header__iconmenu" id="nav-icon2">
                <span></span><span></span><span></span><span></span><span></span><span></span></div>
        </div>
        <div class="header__mobileitem">
            <div class="header__logo"><a class="logo-dark" href="home.html"><img src="assets/img/logo.png"
                                                                                 alt="logo"/></a></div>
        </div>
        <div class="header__mobileitem">
            <div class="header-icon-search"><i class="fa fa-search"></i></div>
        </div>
    </div>
</div>
