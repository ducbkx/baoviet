<div>
    <button class="btn btn-link dropdown-item text-uppercase" data-toggle="dropdown" aria-expanded="false">
        <i class="icon-globe m-r-xs"></i>
        <span id="code-local">{{ Localization::getCurrentLocaleEntity()->native() }}</span>
    </button>

    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
        @foreach(Localization::getSupportedLocales() as $code => $locale)
            <a class="dropdown-item @if ($code === Localization::getCurrentLocale())active @endif"
               href="#local-{{$code}}">{{$locale->native()}}
            </a>
        @endforeach
    </div>
</div>
