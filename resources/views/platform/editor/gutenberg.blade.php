<div id="editor"></div>

@push('stylesheets')
    <link rel="stylesheet" href="{{ asset('vendor/gutenberg/gutenberg.css') }}">

    <script src="https://unpkg.com/react@16.8.6/umd/react.development.js"></script>
    <script src="https://unpkg.com/react-dom@16.8.6/umd/react-dom.development.js"></script>
    <script src="https://unpkg.com/moment@2.22.1/min/moment.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>

    <style>
        .app > .aside {
            margin-left: 0 !important;
            max-width: 160px !important;
            flex: 0 0 160px !important;
            overflow: hidden !important;
        }

        .app > .bg-white {
            flex: 1 0 0 !important;
            max-width: 100% !important;
        }

        .wrapper.mt-4 {
            display: none;
        }

        .edit-post-header {
            top: 0 !important;
        }

        .edit-post-sidebar,
        .edit-post-layout__content {
            top: 55px !important;
        }

        .edit-post-layout {
            padding-top: 0 !important;
        }
    </style>
@endpush

@push('scripts')
    <script src="{{ asset('vendor/gutenberg/gutenberg.js') }}"></script>
@endpush
