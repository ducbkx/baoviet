<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel='dns-prefetch' href='//fonts.googleapis.com'/>

    <title>@yield('title', 'Bảo Việt Nhân Thọ')</title>
    <meta name="description" content="@yield('description', '')">
    <meta name="keywords" content="@yield('keywords', '')">

    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    @yield('meta')

    @section('styles')
        <link rel="stylesheet" href="https://use.typekit.net/mcq6uph.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700">
        <link rel="stylesheet" href="{{ asset('fonts/fontawesome/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('fonts/btec-icon/btec-icon.css') }}">

        <link rel="stylesheet" href="{{ asset('css/vendor.css') }}">
        <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    @show

    @yield('header_scripts')
</head>
<body>
    <div class="page-wrap" id="root">
        <header class="header" id="header">
            @include('partials.header')
        </header>

        <div class="header-search">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-xs-offset-0 col-sm-offset-0 col-md-offset-0 col-lg-offset-2 ">
                        <form class="header-search__form">
                            <input class="form-control" type="text" placeholder="Nhập thông tin cần tìm kiếm…"/><span
                                class="form-icon"><i class="fa fa-search"></i></span><span class="form-close"><i
                                    class="fa fa-close"></i></span>
                        </form>
                        <div class="header-search__keyword">
                            <h4 class="header-search__titlemin">Từ khoá phổ biến</h4>
                            <ul class="header-search__listkey">
                                <li><a href="#">Chăm sóc sức khoẻ</a></li>
                                <li><a href="#">Vượt qua biến cố bệnh tật</a></li>
                                <li><a href="#">Bảo vệ người trụ cột</a></li>
                                <li><a href="#">Tương lai cho con trẻ</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- menu-mobile -->
        <div class="menu-mobile">
            <div class="menu-mobile__inner">
                <nav class="menu-mobile__nav" role="navigation">
                    <ul class="menu-list">
                        <li class="menu-item-current menu-item-has-children"><a href="#">Giải pháp cho bạn</a><span
                                class="menu-item-icon"><i class="fa fa-angle-down"></i></span>
                            <ul class="submenu">
                                <li><a href="#">Chăm sóc sức khỏe</a></li>
                                <li><a href="#">Tạo dựng tài sản</a></li>
                                <li><a href="#">Bảo vệ người trụ cột</a></li>
                                <li><a href="#">Tương lai cho con trẻ</a></li>
                                <li><a href="#">Vượt qua biến cố bệnh tật</a></li>
                                <li><a href="#">Chuẩn bị giai đoan hưu trí</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children"><a href="#">Sản phẩm</a><span class="menu-item-icon"><i
                                    class="fa fa-angle-down"></i></span>
                            <ul class="submenu">
                                <li><a href="#">Dành cho doanh nghiệp</a></li>
                                <li><a href="#">THưu trí</a></li>
                                <li><a href="#">Liên kết ngân hàng</a></li>
                                <li><a href="#">Tích lũy</a></li>
                                <li><a href="#">Bảo vệ</a></li>
                                <li><a href="#">Chuẩn bị giai đoan hưu trí</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children"><a href="#">Chăm sóc khách hàng</a><span
                                class="menu-item-icon"><i class="fa fa-angle-down"></i></span>
                            <ul class="submenu">
                                <li><a href="#">Thay đổi thông tin hợp đồng bảo hiểm</a></li>
                                <li><a href="#">Tra cứu thông tin</a></li>
                                <li><a href="#">Giải quyết quyền lợi bảo hiểm</a></li>
                                <li><a href="#">Tích lũy</a></li>
                                <li><a href="#">Những câu hỏi thường gặp</a></li>
                                <li><a href="#">Thanh toán phí bảo hiểm</a></li>
                            </ul>
                        </li>
                        <li class="menu-item-has-children"><a href="#">Kinh nghiệm</a><span class="menu-item-icon"><i
                                    class="fa fa-angle-down"></i></span>
                            <ul class="submenu">
                                <li><a href="#">Thay đổi thông tin hợp đồng bảo hiểm</a></li>
                                <li><a href="#">Tra cứu thông tin</a></li>
                                <li><a href="#">Giải quyết quyền lợi bảo hiểm</a></li>
                                <li><a href="#">Tích lũy</a></li>
                                <li><a href="#">Những câu hỏi thường gặp</a></li>
                                <li><a href="#">Thanh toán phí bảo hiểm</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Bảo Việt nhân thọ</a></li>
                    </ul>
                </nav>
                <div class="menu-mobile__info"><a href="#"><span> <i class="fa fa-phone"></i>1900 558899<small>– nhánh số 4 </small></span></a><a
                        href="#"><span><i class="fa fa-phone"></i>Chi nhánh</span></a></div>
            </div>
            <a class="menu-mobile__user" href="#"><i class="fa fa-user-circle-o"></i>MyBV Life</a>
        </div><!-- End / menu-mobile -->

        <div class="md-content">
            @yield('content')
        </div>

        <div class="footer">
            @include('partials.footer')
        </div>
    </div>

    @section('scripts')
        <script src="{{ asset('js/vendor.js') }}"></script>
        <script src="{{ asset('js/main.js') }}"></script>
    @show
</body>
</html>
