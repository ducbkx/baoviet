<?php

return [
    /*
    |--------------------------------------------------------------------------
    | The routes permalinks.
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the routes translation.
    | Please do not change any of {pattern} string, they're route parameters.
    |
    | Core terms: {page}, {about}
    */

    'about'      => 'about',
    'about.show' => 'about/{about}',

];
