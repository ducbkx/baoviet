/**
 * External dependencies
 */
import classnames from 'classnames';

/**
 * WordPress dependencies
 */
import {
    Popover,
    ScrollLock,
    FocusReturnProvider,
    navigateRegions,
} from '@wordpress/components';

import {
    BlockInspector,
    PreserveScrollInReorder
} from '@wordpress/block-editor';

import {
    EditorNotices,
    UnsavedChangesWarning,
} from '@wordpress/editor';

import {compose} from '@wordpress/compose';
import {withDispatch, withSelect} from '@wordpress/data';

import {Fragment} from '@wordpress/element';
import {PluginArea} from '@wordpress/plugins';
import {withViewportMatch} from '@wordpress/viewport';

import {PanelBody} from "@wordpress/components/build-module/panel/body";
import OptionsModal from "@wordpress/edit-post/build-module/components/options-modal";
import FullscreenMode from "@wordpress/edit-post/build-module/components/fullscreen-mode";
import ManageBlocksModal from '@wordpress/edit-post/build-module/components/manage-blocks-modal';
import KeyboardShortcutHelpModal from '@wordpress/edit-post/build-module/components/keyboard-shortcut-help-modal';
import EditorModeKeyboardShortcuts from '@wordpress/edit-post/build-module/components/keyboard-shortcuts';
import TextEditor from '@wordpress/edit-post/build-module/components/text-editor';
import VisualEditor from './VisualEditor';

import Header from "./Header";

function Layout({
    mode,
    editorSidebarOpened,
    pluginSidebarOpened,
    publishSidebarOpened,
    hasFixedToolbar,
    closePublishSidebar,
    togglePublishSidebar,
    hasActiveMetaboxes,
    isSaving,
    isMobileViewport,
    isRichEditingEnabled,
}) {
    const sidebarIsOpened = editorSidebarOpened || pluginSidebarOpened || publishSidebarOpened;

    const className = classnames('edit-post-layout', {
        'is-sidebar-opened': sidebarIsOpened,
        'has-fixed-toolbar': hasFixedToolbar,
    });

    return (
        <FocusReturnProvider className={className}>
            {/*<FullscreenMode/>*/}
            {/*<UnsavedChangesWarning/>*/}

            <Header/>

            <div
                className="edit-post-layout__content"
                role="region"
                aria-label={'Editor content'}
                tabIndex="-1"
            >
                <EditorNotices dismissible={false} className="is-pinned"/>
                <EditorNotices dismissible={true}/>

                {/*<PreserveScrollInReorder/>*/}
                {/*<EditorModeKeyboardShortcuts/>*/}
                {/*<KeyboardShortcutHelpModal/>*/}
                {/*<ManageBlocksModal/>*/}
                {/*<OptionsModal/>*/}

                {(mode === 'text' || !isRichEditingEnabled) && <TextEditor/>}
                {isRichEditingEnabled && mode === 'visual' && <VisualEditor/>}
            </div>

            <div
                className="edit-post-sidebar"
                role="region"
                tabIndex="-1"
            >
                <PanelBody className="edit-post-settings-sidebar__panel-block">
                    <BlockInspector/>
                </PanelBody>
            </div>

            <Popover.Slot/>
        </FocusReturnProvider>
    );
}

export default compose(
    withSelect((select) => {
        return {
            mode: select('core/edit-post').getEditorMode(),
            editorSidebarOpened: select('core/edit-post').isEditorSidebarOpened(),
            pluginSidebarOpened: select('core/edit-post').isPluginSidebarOpened(),
            publishSidebarOpened: select('core/edit-post').isPublishSidebarOpened(),
            hasFixedToolbar: select('core/edit-post').isFeatureActive('fixedToolbar'),
            hasActiveMetaboxes: select('core/edit-post').hasMetaBoxes(),
            isSaving: select('core/edit-post').isSavingMetaBoxes(),
            isRichEditingEnabled: select('core/editor').getEditorSettings().richEditingEnabled,
        }
    }),
    withDispatch((dispatch) => {
        const {closePublishSidebar, togglePublishSidebar} = dispatch('core/edit-post');

        return {
            closePublishSidebar,
            togglePublishSidebar,
        };
    }),
    navigateRegions,
    withViewportMatch({isMobileViewport: '< small'}),
)(Layout);
