import {
    VisualEditorGlobalKeyboardShortcuts,
} from '@wordpress/editor';

import {
    WritingFlow,
    ObserveTyping,
    BlockList,
    CopyHandler,
    BlockSelectionClearer,
    MultiSelectScrollIntoView,
} from '@wordpress/block-editor';

function VisualEditor() {
    return (
        <BlockSelectionClearer className="edit-post-visual-editor editor-styles-wrapper">
            <VisualEditorGlobalKeyboardShortcuts/>
            <MultiSelectScrollIntoView/>

            <WritingFlow>
                <ObserveTyping>
                    <CopyHandler>
                        <BlockList/>
                    </CopyHandler>
                </ObserveTyping>
            </WritingFlow>
        </BlockSelectionClearer>
    );
}

export default VisualEditor;
