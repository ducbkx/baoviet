import {pick} from 'lodash';
import memize from 'memize';

import {compose} from '@wordpress/compose';
import {Component} from '@wordpress/element';
import {withDispatch, withSelect} from '@wordpress/data';
import {BlockEditorProvider} from '@wordpress/block-editor';

class EditorProvider extends Component {
    constructor(props) {
        super(...arguments);

        const post = {
            title: '',
            content: {
                raw: ''
            },
        };

        props.setupEditor(post);
    }

    componentDidMount() {
        this.props.updateEditorSettings(this.props.settings);
    }

    componentDidUpdate(prevProps) {
        if (this.props.settings !== prevProps.settings) {
            this.props.updateEditorSettings(this.props.settings);
        }
    }

    render() {
        const {
            children,
            blocks,
            resetEditorBlocks,
            isReady,
            resetEditorBlocksWithoutUndoLevel,
        } = this.props;

        if (!isReady) {
            return null;
        }

        return (
            <BlockEditorProvider
                value={blocks}
                onInput={resetEditorBlocksWithoutUndoLevel}
                onChange={resetEditorBlocks}
            >
                {children}
            </BlockEditorProvider>
        );
    }
}

export default compose([
    withSelect((select) => {
        const {
            __unstableIsEditorReady: isEditorReady,
            getEditorBlocks,
        } = select('core/editor');

        return {
            isReady: isEditorReady(),
            blocks: getEditorBlocks(),
        };
    }),
    withDispatch((dispatch) => {
        const {
            setupEditor,
            resetEditorBlocks,
            updateEditorSettings,
        } = dispatch('core/editor');

        return {
            setupEditor,
            resetEditorBlocks,
            updateEditorSettings,
            resetEditorBlocksWithoutUndoLevel(blocks) {
                resetEditorBlocks(blocks, {
                    __unstableShouldCreateUndoLevel: false,
                });
            }
        };
    }),
])(EditorProvider);
