import HeaderToolbar from './HeaderToolbar';

import {select, withDispatch, withSelect} from '@wordpress/data';
import MoreMenu from '@wordpress/edit-post/build-module/components/header/more-menu';
import PinnedPlugins from '@wordpress/edit-post/build-module/components/header//pinned-plugins';
import {compose} from "@wordpress/compose";

function Header({onSave}) {
    return (
        <div
            role="region"
            className="edit-post-header"
            tabIndex="-1"
        >
            <HeaderToolbar/>

            <div className="edit-post-header__settings">
                <button onClick={onSave}>Save</button>
                <PinnedPlugins.Slot/>
                <MoreMenu/>
            </div>
        </div>
    );
}

export default compose(
    withSelect((select) => {
        const onSave = () => {
            console.log(select('core/editor').getEditedPostContent())
        };

        return {
            onSave: onSave
        }
    }),
)(Header);
