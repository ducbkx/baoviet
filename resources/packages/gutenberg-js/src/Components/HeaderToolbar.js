import {compose} from "@wordpress/compose";
import {withSelect} from "@wordpress/data";
import {withViewportMatch} from "@wordpress/viewport";

import {
    Inserter,
    NavigableToolbar,
    BlockNavigationDropdown, BlockToolbar,
} from '@wordpress/block-editor';

import {
    TableOfContents,
    EditorHistoryRedo,
    EditorHistoryUndo,
} from '@wordpress/editor';

import FullscreenModeClose from "@wordpress/edit-post/build-module/components/header/fullscreen-mode-close";

function HeaderToolbar({hasFixedToolbar, isLargeViewport, showInserter, isTextModeEnabled}) {
    return (
        <NavigableToolbar className="edit-post-header-toolbar">
            {/*<FullscreenModeClose/>*/}

            <div>
                <Inserter disabled={!showInserter} position="bottom right"/>
            </div>

            <EditorHistoryUndo/>
            <EditorHistoryRedo/>

            <TableOfContents hasOutlineItemsDisabled={isTextModeEnabled}/>
            <BlockNavigationDropdown isDisabled={isTextModeEnabled}/>

            {hasFixedToolbar && isLargeViewport && (
                <div className="edit-post-header-toolbar__block-toolbar">
                    <BlockToolbar/>
                </div>
            )}
        </NavigableToolbar>
    );
}

export default compose([
    withSelect((select) => ({
        hasFixedToolbar: select('core/edit-post').isFeatureActive('fixedToolbar'),
        // This setting (richEditingEnabled) should not live in the block editor's setting.
        showInserter: select('core/edit-post').getEditorMode() === 'visual' && select('core/editor').getEditorSettings().richEditingEnabled,
        isTextModeEnabled: select('core/edit-post').getEditorMode() === 'text',
    })),
    withViewportMatch({isLargeViewport: 'medium'}),
])(HeaderToolbar);
