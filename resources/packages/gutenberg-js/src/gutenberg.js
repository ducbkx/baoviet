import '@babel/polyfill'

import '@wordpress/core-data';
import '@wordpress/block-editor';
import '@wordpress/editor';
import '@wordpress/nux';
import '@wordpress/viewport';
import '@wordpress/notices';
import '@wordpress/format-library';

import domReady from '@wordpress/dom-ready'
import {Fragment, render} from '@wordpress/element';
import {registerCoreBlocks} from '@wordpress/block-library';

import {ErrorBoundary} from '@wordpress/editor';
import {KeyboardShortcuts} from '@wordpress/components';
import preventEventDiscovery from '@wordpress/edit-post/build-module/prevent-event-discovery';

import apiFetch from '@wordpress/api-fetch';
apiFetch.setFetchHandler(function () {
    return new Promise(function(resolve) {
        resolve()
    });
});

import * as data from '@wordpress/data';
data.use(data.plugins.persistence, {storageKey: 'WP_DATA_USER_1'});
data.use(data.plugins.controls);

import '@wordpress/edit-post/build-module/store';

import Layout from "./Components/Layout";
import EditorProvider from "./Components/EditorProvider";

function Gutenberg() {
    const onError = function () {
        
    };

    return (
        <Fragment>
            <EditorProvider>
                <ErrorBoundary onError={onError}>
                    <Layout/>
                    {/*<KeyboardShortcuts shortcuts={preventEventDiscovery}/>*/}
                </ErrorBoundary>
            </EditorProvider>
        </Fragment>
    )
}

function initializeEditor() {
    registerCoreBlocks();

    render(
        <Gutenberg/>,
        document.querySelector('#editor')
    );
}

const initEditor = new Promise(function (resolve) {
    domReady(() => {
        resolve(initializeEditor());
    });
})
