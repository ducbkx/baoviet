let mix = require('laravel-mix')

/**
 * The webpack externals library.
 *
 * @type {object}
 */
const externals = {
    wp: 'wp',
    'react': 'React',
    'react-dom': 'ReactDOM',
    'jquery': 'jQuery',
    'moment': 'moment',
    // 'lodash': 'lodash',
};

mix.setPublicPath('dist')
    .react('src/gutenberg.js', 'dist')
    .sass('src/scss/gutenberg.scss', 'dist')
    .sourceMaps();

mix.disableSuccessNotifications();

mix.webpackConfig({
    externals
});

mix.options({
    processCssUrls: false
});
