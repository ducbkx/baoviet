(function ($) {
    "use strict";

    /**
     * [isMobile description]
     * @type {Object}
     */
    window.isMobile = {
        Android: function Android() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function BlackBerry() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function iOS() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function Opera() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function Windows() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function any() {
            return isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows();
        }
    };
    window.isIE = /(MSIE|Trident\/|Edge\/)/i.test(navigator.userAgent);
    window.windowHeight = window.innerHeight;
    window.windowWidth = window.innerWidth;

    /**
     * [debounce description]
     * @param  {[type]} func      [description]
     * @param  {[type]} wait      [description]
     * @param  {[type]} immediate [description]
     * @return {[type]}           [description]
     */
    function debounce(func, wait, immediate) {
        var timeout;
        return function () {
            var context = this,
                args = arguments;
            var later = function later() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    }

    /**
     * Countdown Circle
     */
    $('.countdown__circle').each(function () {
        var self = $(this);

        self.TimeCircles({
            "animation": "smooth",
            "bg_width": 0.3,
            "fg_width": 0.03,
            "circle_bg_color": "#ccc",
            "time": {
                "Days": {
                    "text": "Days",
                    "color": "#000",
                    "show": true
                },
                "Hours": {
                    "text": "Hours",
                    "color": "#000",
                    "show": true
                },
                "Minutes": {
                    "text": "Minutes",
                    "color": "#000",
                    "show": true
                },
                "Seconds": {
                    "text": "Seconds",
                    "color": "#000",
                    "show": true
                }
            }

        });
    });

    // class hero {
    // constructor() {
    // 	this.css = {"module":".hero","slide":".hero__slide","wrapper":".hero__wrapper","thumb":".hero__thumb","title":".hero__title","text":".hero__text","":""};
    // 	this.apply();
    // }

    // apply() {
    // 	const { css } = this;
    // 	this.svg(css);
    // }

    // svg(css) {
    // 	const top = $(css.svg).find('path');

    // 	$(window).on('load', function() {
    // 		TweenMax.to(top, 10, {
    // 			attr:{
    // 				d: "M12-11c342,64,514,22,268,300C-58,707,408.2,439.6,746,489c337.8,49.5-281.6,470.8-28,508  c169.1,24.8,207.8,19.3,304,65.9V-11H12z"
    // 			},
    // 			ease: Power1.easeInOut,
    // 			repeat:-1,
    // 			yoyo:true
    // 		});
    // 	})
    // }
    // }
    // new hero();


    /*
  * headerPage
  */
    function headerPage() {
        var _module = $('.header');

        function handleFix() {
            var fixheader = 'header__fixheight';

            var appendFix = debounce(function () {
                if (_module.length) {

                    var hHeader = _module.outerHeight(),
                        headerFix = "<div class=\"fixheight-header\"></div>";

                    if ($('.fixheight-header').length === 0) _module.closest('.page-wrap').prepend(headerFix);
                    $('.fixheight-header').css('height', hHeader + "px");
                }
            }, 100);
            appendFix();
            $(window).on('resize', appendFix);
        }

        function toggleMenu() {
            var toggle = $('.header__iconmenu');

            toggle.on('click', function (e) {
                e.stopPropagation();
                var self = $(this);
                self.toggleClass('toggle-active');
                $('.menu-mobile').toggleClass('menu-mobile--active');
                $('body').toggleClass('body-fix-overflow');

                $('body').on('click', function () {
                    self.removeClass('toggle-active');
                    $('.menu-mobile').removeClass('menu-mobile--active');
                });
            });
        }

        function ScrollEffect() {
            var scrollfix = function scrollfix() {
                var scrollTop = $(window).scrollTop(),
                    heightInfo = $('.header__info').height(),
                    offset = 0;

                if (scrollTop <= heightInfo) {
                    offset = scrollTop;
                } else {
                    offset = heightInfo;
                }

                _module.css({
                    'transform': 'translateY(-' + offset + 'px)'
                });
            };
            scrollfix();
            $(window).on('scroll', scrollfix);
        }

        function menuMobile() {
            $('.menu-mobile').on('click', function (e) {
                e.stopPropagation();
            });

            $('.menu-mobile .menu-item-has-children .menu-item-icon').on('click', function (e) {
                e.preventDefault();
                var self = $(this);
                self.closest('li').toggleClass('active-submneu');
                self.next().slideToggle();
            });
        }

        function headerSearch() {
            $('.header-icon-search').on('click', function () {
                $('.header-search').addClass('header-search--active');

                $(this).closest('header').addClass('header-show-search');
            });

            $('.header-search__form .form-close').on('click', function () {
                $('.header-search').removeClass('header-search--active');
                $('.header').removeClass('header-show-search');
            });
        }

        function headerScroll() {
            if (_module.length) {
                var headeroom = new Headroom(document.querySelector("header"), {
                    tolerance: 4,
                    offset: 100,
                    classes: {
                        pinned: "header-pin",
                        unpinned: "header-unpin"
                    },
                    onPin: function onPin() {
                    },
                    onUnpin: function onUnpin() {
                    }
                });
                headeroom.init();
            }
        }

        handleFix();
        // headerScroll();
        toggleMenu();
        ScrollEffect();
        menuMobile();
        headerSearch();
    }

    /**
     * swiperSlides
     */
    function swiperSlides() {
        $('.swiper__module').each(function () {
            var self = $(this),
                wrapper = $('.swiper-wrapper', self),
                optData = eval('(' + self.attr('data-options') + ')'),
                optDefault = {
                    speed: 700,
                    pagination: {
                        el: self.find('.swiper-pagination-custom'),
                        clickable: true
                    },
                    navigation: {
                        nextEl: self.find('.swiper-button-next-custom'),
                        prevEl: self.find('.swiper-button-prev-custom')
                    },
                    spaceBetween: 30
                },
                options = $.extend(true, optDefault, optData);
            wrapper.children().wrap('<div class="swiper-slide"></div>');
            var swiper = new Swiper(self, options);

            function thumbnails(selector) {

                if (selector.length > 0) {
                    var wrapperThumbs = selector.children('.swiper-wrapper'),
                        optDataThumbs = eval('(' + selector.attr('data-options') + ')'),
                        optDefaultThumbs = {
                            spaceBetween: 10,
                            speed: 700,
                            centeredSlides: true,
                            slidesPerView: 3,
                            touchRatio: 0.3,
                            slideToClickedSlide: true,
                            pagination: {
                                el: selector.find('.swiper-pagination-custom'),
                                clickable: true
                            },
                            navigation: {
                                nextEl: selector.find('.swiper-button-next-custom'),
                                prevEl: selector.find('.swiper-button-prev-custom')
                            }
                        },
                        optionsThumbs = $.extend(optDefaultThumbs, optDataThumbs);
                    wrapperThumbs.children().wrap('<div class="swiper-slide"></div>');
                    var swiperThumbs = new Swiper(selector, optionsThumbs);
                    swiper.controller.control = swiperThumbs;
                    swiperThumbs.controller.control = swiper;
                }
            }

            thumbnails(self.next('.swiper-thumbnails__module'));
        });
    }

    /**
     * HeroSlide
     */
    function heroSlide() {
        var slide = $('.hero__slide'),
            thumb = $('.hero__thumb');

        if (slide.length) {
            var slides = new Swiper(slide, {
                speed: 700,
                spaceBetween: 30,
                effect: 'fade'
            });

            var thumbs = new Swiper(thumb, {
                speed: 700,
                touchRatio: 0.3,
                slideToClickedSlide: true,
                pagination: {
                    el: thumb.find('.swiper-pagination-custom'),
                    clickable: true
                }
            });

            slides.controller.control = thumbs;
            thumbs.controller.control = slides;
        }
    }

    /**
     * slideYearProcess
     */
    function slideYearProcess() {
        var wrap = $('.slide-year-process-wrapper'),
            slide = wrap.find('.slide-year-process');

        if (slide.length) {
            var slides = new Swiper(slide, {
                slidesPerView: 4,
                spaceBetween: 30,
                scrollbar: {
                    el: wrap.find('.swiper-pagination-scrollbar'),
                    draggable: true
                },
                breakpoints: {
                    1200: {
                        slidesPerView: 3,
                        spaceBetween: 30
                    },
                    991: {
                        slidesPerView: 2,
                        spaceBetween: 20
                    },
                    500: {
                        slidesPerView: 1,
                        spaceBetween: 10
                    }
                }
            });

            slide.find('.swiper-slide').matchHeight();

            var myEfficientFn = debounce(function () {
                slide.find('.swiper-slide').matchHeight();
            }, 250);

            window.addEventListener('resize', myEfficientFn);
        }
    }

    /**
     * gridJs
     */
    function gridJs() {
        $('.grid-css').mdGridJs();

        $('.grid__filter-project').each(function () {
            var self = $(this);
            var inner = self.closest('.md-section').find('.grid__inner');

            self.on('click', 'a', function (e) {
                e.preventDefault();
                var $el = $(this);
                var selector = $el.attr('data-filter');
                self.find('.current').removeClass('current');
                $el.parent().addClass('current');
                inner.isotope({
                    filter: selector
                });
            });
        });

        $('.grid-css--popup-album').each(function () {
            var self = $(this),
                inner = $('.grid__inner', self);

            inner.magnificPopup({
                delegate: 'a',
                type: 'image',
                tLoading: 'Loading image #%curr%...',
                overflowY: 'scroll',
                image: {
                    markup: "<div class=\"mfp-figure\">\n\t\t\t\t\t\t\t<div class=\"mfp-close\"></div>\n\t\t\t\t            <div class=\"mfp-image\">\n\t\t\t\t            \t<div class=\"mfp-img\"></div>\n\t\t\t\t            </div>\n\t\t\t\t            <div class=\"mfp-bottom-bar\">\n\t\t\t\t              \t<div class=\"mfp-title\"></div>\n\t\t\t\t              \t<div class=\"mfp-counter\"></div>\n\t\t\t\t           \t</div>\n\t\t\t\t        </div>",
                    tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                    titleSrc: function titleSrc(item) {
                        return item.el.attr('title');
                    }
                },
                gallery: {
                    enabled: true,
                    navigateByImgClick: true,
                    preload: [0, 1]
                },
                removalDelay: 500,
                showCloseBtn: true,
                closeOnContentClick: false,
                closeBtnInside: true,
                callbacks: {
                    beforeOpen: function beforeOpen() {
                        this.st.mainClass = this.st.el.attr('data-effect');
                    },

                    buildControls: function buildControls() {
                        this.contentContainer.find('.mfp-image').append(this.arrowLeft);
                        this.contentContainer.find('.mfp-image').append(this.arrowRight);
                    }
                },
                midClick: true
            });
        });
    }

    /**
     * popupJs
     */
    function popupJs() {
        var magnificPopupDefault = {
            type: 'image',
            overflowY: 'scroll',
            image: {
                markup: "<div class=\"mfp-figure\">\n\t\t\t\t\t\t<div class=\"mfp-close\"></div>\n\t\t\t            <div class=\"mfp-image\">\n\t\t\t            \t<div class=\"mfp-img\"></div>\n\t\t\t            </div>\n\t\t\t            <div class=\"mfp-bottom-bar\">\n\t\t\t              \t<div class=\"mfp-title\"></div>\n\t\t\t           \t</div>\n\t\t\t        </div>",
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                tCounter: false,
                titleSrc: function titleSrc(item) {
                    return item.el.attr('title');
                }
            },
            removalDelay: 500,
            showCloseBtn: false,
            closeOnBgClick: true,
            closeBtnInside: true,
            callbacks: {
                beforeOpen: function beforeOpen() {
                    this.st.mainClass = this.st.el.attr('data-effect') ? this.st.el.attr('data-effect') : 'mfp-zoom-in';
                }
            },
            midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
        };

        $('[data-init="magnificPopup"]').each(function (index, el) {
            var $el = $(this);

            $el.magnificPopup(magnificPopupDefault);
        });

        $('[data-init="magnificPopupVideo"]').each(function (index, el) {
            var $el = $(this);
            var option = {
                type: 'iframe',
                disableOn: 500

                // Merge settings.
            };
            var settings = $.extend(true, magnificPopupDefault, option);

            $el.magnificPopup(settings);
        });

        $('[data-init="magnificPopupInline"]').each(function (index, el) {
            var $el = $(this);
            var option = {
                type: 'inline',
                removalDelay: 200

                // Merge settings.
            };
            var settings = $.extend(true, magnificPopupDefault, option);

            $el.magnificPopup(settings);
        });

        $('[data-init="magnificPopupAjax"]').each(function (index, el) {
            var $el = $(this);
            var option = {
                type: 'ajax'

                // Merge settings.
            };
            var settings = $.extend(true, magnificPopupDefault, option);

            $el.magnificPopup(settings);
        });
    }

    /**
     * Call function
     */
    function countTos() {
        var module = $('.countTo');

        module.each(function () {
            var self = $(this),
                countNumber = $('.countTo__number', self),
                unit = self.data('unit');

            var optData = eval('(' + self.attr('data-options') + ')'),
                optDefault = {
                    from: 0,
                    to: 20,
                    speed: 2000,
                    refreshInterval: 50,
                    formatter: function formatter(value, options) {
                        if (value.toFixed(options.decimals) < 10) {
                            return '0' + value.toFixed(options.decimals);
                        } else {
                            return value.toFixed(options.decimals);
                        }
                    }
                },
                options = $.extend(optDefault, optData);

            self.waypoint(function (direction) {
                countNumber.countTo(options);

                this.destroy();
            }, {
                offset: function offset() {
                    return Waypoint.viewportHeight() - self.outerHeight() - 150;
                }
            });
        });
    }

    /**
     * Accordions
     */
    function accordionJs() {
        $('.ac-accordion').each(function () {
            var self = $(this),
                optData = eval('(' + self.attr('data-options') + ')'),
                optDefault = {
                    active: 0,
                    collapsible: false,
                    activeEvent: 'click',
                    heightStyle: 'content',
                    duration: 200,
                    onOffIcon: {
                        enable: true,
                        expandIcon: "fa fa-chevron-up",
                        collapseIcon: "fa fa-chevron-down",
                        position: "right"
                    }
                },
                options = $.extend(optDefault, optData);
            self.aweAccordion(options);
        });
    }

    /**
     * Tabs
     */
    function tabJs() {
        $('.tabs__module').each(function () {
            var self = $(this),
                optData = eval('(' + self.attr('data-options') + ')'),
                optDefault = {
                    active: 0,
                    activeEvent: 'click',
                    navigatorPosition: 'left'
                },
                options = $.extend(optDefault, optData);
            self.aweTabs(options);
        });
    }

    /**
     * numberTextLines
     */
    function numberTextLines() {
        $.fn.numberTextLine = function (opts) {
            $(this).each(function () {
                var el = $(this),
                    defaults = {
                        numberLine: 0
                    },
                    data = el.data(),
                    dataTemp = $.extend(defaults, opts),
                    options = $.extend(dataTemp, data);

                if (!options.numberLine) return false;

                el.bind('customResize', function (event) {
                    event.stopPropagation();
                    reInit();
                }).trigger('customResize');
                $(window).resize(function () {
                    el.trigger('customResize');
                });

                function reInit() {
                    var fontSize = parseInt(el.css('font-size')),
                        lineHeight = parseInt(el.css('line-height')),
                        overflow = fontSize * (lineHeight / fontSize) * options.numberLine;

                    el.css({
                        'display': 'block',
                        'max-height': overflow,
                        'overflow': 'hidden'
                    });
                }
            });
        };

        $('[data-number-line]').numberTextLine();
    }

    /**
     * New hover Effect
     */
    function hoverNewEffect() {
        $('.new__fix').each(function () {
            var self = $(this),
                text = $('.new__text', self);

            var windowResize = function windowResize() {
                var wh = text.outerHeight();
                self.css({
                    'transform': 'translateY(' + wh + 'px)'
                });
            };
            windowResize();
            $(window).on('resize', windowResize);
        });
    }

    /**
     * footerScroll
     */
    function footerScroll() {
        var fixeds = function fixeds() {
            var wh = $(window).height(),
                scroll = $(window).scrollTop();

            if (scroll >= wh + 100) {
                $('.footer-fixed').addClass('footer-fixed-show');
            } else {
                $('.footer-fixed').removeClass('footer-fixed-show');
            }
        };
        fixeds();
        $(window).on('scroll', fixeds);
    }

    /**
     * language
     */
    function languageJs() {
        function formatState(state) {
            if (!state.id) {
                return state.text;
            }
            var baseUrl = "img/lang/";
            var $state = $('<span><img src="' + baseUrl + '/' + state.element.value.toLowerCase() + '.png" class="img-flag" /> ' + state.text + '</span>');
            return $state;
        };

        $(".footer__languages").select2({
            minimumResultsForSearch: Infinity,
            templateResult: formatState,
            templateSelection: formatState,
            theme: 'language'
        });
    }

    /**
     * heroSelect
     */
    function heroSelect() {
        function formatState(state) {
            if (!state.id) {
                return state.text;
            }
            var baseUrl = $(state.element).attr('data-img');

            var $state = $('<span><img src="' + baseUrl + '" class="img-flag" /> ' + state.text + '</span>');
            return $state;
        };

        $('.hero-search .form-control').select2({
            minimumResultsForSearch: Infinity,
            templateResult: formatState,
            templateSelection: formatState,
            theme: 'hero'
        });
    }

    /**
     * selecJs
     */
    function selecJs() {
        $('.select2Js').select2({
            minimumResultsForSearch: Infinity,
            theme: 'select'
        });
    }

    /**
     * Match height
     */
    function fixHiehgtJs() {
        $('.row-eq-height > [class*="col-"]').matchHeight();

        var myEfficientFn = debounce(function () {
            $('.row-eq-height > [class*="col-"]').matchHeight();
        }, 250);

        window.addEventListener('resize', myEfficientFn);
    }

    /**
     * Match height
     */
    function menuBoxMin() {
        var inner = $('.one-page-nav'),
            fix = $('.header__inner').height();

        inner.onePageNav({
            currentClass: 'current',
            changeHash: false,
            scrollSpeed: 750,
            scrollThreshold: 0.5,
            scrollOffset: fix,
            filter: '',
            easing: 'swing'
        });
    }

    /**
     * handlingAjax
     */
    function handlingAjax() {

        // ajax-reg-successful
        $('.ajax-reg-successful').on('click', function (e) {
            e.preventDefault();
            $.magnificPopup.close();

            // test js
            var random = Math.floor(Math.random() * 10);

            var html;
            if (random % 2 == 0) {
                html = "\n\t\t\t\t<div class=\"form-successful\">\n\t\t\t\t\t<div class=\"form-successful__icon\">\n\t\t\t\t\t\t<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QUUzOTI3NkM0OTMxMTFFOUJBNkJCNThGRTE1NkE4QTYiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QUUzOTI3NkQ0OTMxMTFFOUJBNkJCNThGRTE1NkE4QTYiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpBRTM5Mjc2QTQ5MzExMUU5QkE2QkI1OEZFMTU2QThBNiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpBRTM5Mjc2QjQ5MzExMUU5QkE2QkI1OEZFMTU2QThBNiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pih9GQ4AAA4KSURBVHjazFt7cFTVGf/OuZtsgE0IBiUPCamMYQYUtIptaU2bmQKWSBIeLakzHan2BdMZwb+cYvuHw3TsP4bpTMGpVvlLQ2tLGBoS8BGNNlNBNMHqVDuUEGIIL0HYhF2y955+33ncxz6STZOYnMzJ3Xv33nPP73t/3znLhBCQ3OgKM0f8J5g+x3sZY/J686kh2Hnsc/jgYgyvM7wuwD8W3rcQz5fhxcV4WokXKvBYir0Ie0TfFsV+CXs/9h7sn2L/GHs39pNqMjg25ziMDffOmwE7vnoT1N82052LO9ksG0sHOAjeAeYgSI4vAA7dl4bhl29fgHf6o/KcJsKYhUeHhluO/6rxiyp8cAV+nkMU4/jnjPKeYOOXkYCdOGYH46xd2OKYpiKCB/hm8UzYXTUP7igK4ascoDfQEZ8bP2DT4jbAjncvQuOJz8GxkaRIBOCSymHkcD1SZS2iqgFmFVoI3mYup0E4jposfc7ifSRDRF4pVYxfwZMWfOFBEixEHMcJQChkwePLZsNTX5sLYaakELJg9siAtTifvBKHja+dg+5z8eQ7GnByDThEHQGauEacckiH3I96/API4SY8bxKamHffkgN/WVUKt+XnZCXao3L476dj8MNXB2AwZiN4IzYOiit/BI+b8aWW1Cet82lfkiVnfbNSuquf43h0tJRgQ1mDvdhfwN5JF/LDFry0shQeXJA3PsAv/vsaPNpxHsQNfAcnEWN0/1b8agu+eonwcyMdh8bRjJEMSI48t8zYH2Hfg+/ajeciZHF4rnoebF4UyRKwlBFP8Ru7v4DH3znvv7cc+3bs22DKmyKolqpdeN7IuNNLUtH4rbnw2NJCfV+qIUvL4b3I2R+/PuC/dBf2J7BvGkl0p7Dtw/40ouki0Hu/WwIPV0YCOm3cGCO3YihG/rSl9zrUt50F27aN6bsP+2/w5pox6eFkNhkXMBeInL+wWxDRU3hyNJRjwf7VxajTM1M4HeDwf68Ow11/PgPXyAdx1BXHJs7uxF4Dyfo0tXjTSxkTLcikJ3GuXRE0ZCcaFkBFfkiTRoHmhgJx5PSGtn6I3lDGhjmJci3GNZI6jI0popnMJgUP/RPTDGBCwxBWjZwzZ+XRhEA8fTDsCB+HHQqVQEYqv+78ArovDsvIikyyYIwM1CaNXtNlmog0TQODG8a1y2PKgKloC+cs+HacK+u6kIAdRy9IsHLmgsuYDP516QY8gxGUEm961NmKd2xzIxfhOtTpIdI4j5JILrz//QWwWuup68KY5Og2UO4TdnVdgw8RH9OMk7KwpeMC2MQ96fBlDLzFiEqKoZoGhqs4EoL22hJYVpQDzWtK4IGKWRKsnKsbY7IteL4ikbBha8cF153x/acG4R/9Qz7lAIygYIk6dVK5+qVwmbsRV/LbSjCEbF9bCovm5Er1C+N89j9QqkAHG2F4hDKtd/pjcKAnqkbeefzzQGyMffO08a6CBayxAVtZmKv8qs7W8ixHctqIt08KNyNRGki3d753VQF+/1zMfBmWgBlYU6+ktrLAPvaWzsp1OSuFzFhoGWpyyemmlSVQkMf9UmiBwxsQfvi9gSi8diYqSWREqB6PddMhjGKOpfQR7QrZEdLZN+pKJFjPFzuewcZ/MUwuNh0egKuxRFANuFNH2JjFYPeH18hKW5Ki2NZKf8WmgyQ7rioXF1jwZm1ZAKzxq6YiM4yI1x86D0f6op4LYpqRaqi1RJTWPuIwkyHkcgRa4zm5qUYsJOGVGN+KOpvjCyM9o0a8iSGgBw8NQGvPVQo8gtEJ6jfn8t4aDCmXxxLkh5VlrkachT5LPdWOFv1sDrxRSzob8iIqFjRiQwh23aHP4PXeQa23QQml+x1ZO4FCDKSqKVgxuVPVdEp9SiOe63ErL5q7JhCKoWCuR7BtvUOo6k7GgExFkvJYpeprAAuxr8goWklB+5fhZxVnc30RFLhcBi3G9a2fweHTQ/p55o0rMhbyVqD0LuQYjy6T1cWk0I2lCTIkffyUHi8RMvhZGVRkiOgGbQ9swOIIoYsCGSsthHEZR2lYnDxreplSZQXQUJdbwqs+gqK8YAKCxi6L0s4oftYf0ZnaM83pegJgI2Z0RzTYAOflkY9mchcT/ytZurtIL7iQVo4KAxTFfLBxvuSCLNGCrB5oI8eDYjVeP6vDegOUXhHHd64/3AdtPde9XAaSi4OjEruSZlchUmSTck0EZjuIzUGwefC3NWVw59wwdKBPLC5QWYkgHy5r02KMkjyKn2WOlh7FwRtajNtOxTAlNM8KVe8em2ep4Hr5I+h+qewjElJMVs+fBc3fK4MZ2sAsRJ/4Zu0CmZ7JiqnhVpbczdbPqljZkWJci2IsdZaI7Khn6dVM+liujJ9ODUeBXkpPFDHuGSujD/ToqvIZMijPs7zv6H9lIXIFRVCCVlGaazQmxs868nPM5ijGqLOnhnzv0OtJ3Fhw2yNWkhFM04rwMR4xbkx4cbUMwvetLoY8nsw1dXPl7LAH2k+sLPLlUf2sDiqUGEe1Qvu9BvfEmKIr4WQrXRFu6rvJgK6is3voyIB08Mk+1ECSoFH/pCHLlC8n6XdWfpY4e0j72cB4PE2R35H1rZGliwc+RUUGzrSevi6pnAzanQLqE+lfe20xFPtAM/89MqYVPj9762T52RE0yB0liuZYXEopwboiBHC4Z1BOgIxHKuHUM4sK8yQQw2npUbjx36pq4olxaDL9bAYb6T51iZKHfp1Ru4GG0Q/m4LllSdDr2s7InFO4BiIwkARCnCbQsmxK1lu6jRD6WR4Q40n0s0ER9tkk3fqJwz1ygtI36hqWvlFYGBE5tjT/h0/HYF3LWRh2TGlUW3Nhu1wlTpNOz5sdkmJEz81Dn/1WXYXreibZzwYJg2Nz6bLctLGHOPypDAQES12hIy5RpOUoN0GZSS0aE7KgoJdIQQ9mpnQ7AuuoKZPRE/W3HixHsCzF+EySn02FThIkXCP0KWN/+M96fPlf00uGkNsdTAnUrNdSmEnBSJ7lhdEiaWHik8s3lCUnPyslCFIqFmQM1yHYtlNDukTssyXu5hIxrhzd7CbQbQNHALSB5HJ68jCZIRk9UrorJDf81ts/pLmX9LWSCm7A06ZUk+RnM2e4jF9GNN00yknkXKfhaEZ9SPKzBHp9a58E7RdX1zL7xDx51In3sxlW3PxEFE4ngj8pAw8kZoe8aI9tzEx+GiC4YuFf7RtyJt7Ppmeto12eMOA7yN5wacEc0Y7XrmS9qpCNn3Yp6wEiwmw81Dfhfjad7VFjuXu5ruCF9nBI6Lo0t45hIt/C3MWobNYrR/bTTGZctltvIj9LBqq1JzYBfja7+Xn1LNGCEzpWU56vaMGcBBmHg4HFqFEtwch+msSH8mUqIMQxr65r6ZcGys1FxuVns1hODUrbQTKeP18aAX7fLTPwQojSrGYEciA7kR7ZTw/anj5eTwhY23oWjpxRpVS1/Wji/GwWDTHx5nuLc2FV6SzgT9w7BydMHGZxfGtTdqbLREPEYaH3QqpJkzHa0NovLbHKZ8/CqyjyXqVyXPnsGI00p8ERkxOnPZrSZtCLqpr74e3+QSMKz2N/dGwlVyepmsHclTxjjc3Cl38TjedzE0rKJkqHvfH/hP0n95fNgI76UlW6IsAnLsbhnlf6IGE7oGvUf8Qvl7h7I+RyjDWBE/pSGm1c+1nI4p3Hf3ArLL0p7O5khDuLcmH7XW5puhO/2QNu/Gm2L01TsCxjtrQHQ9rObctmS7BGm9xtSzHU42/sPwtd52g/hKDrz+CHbUyk2QI4XXDqwEKkxgm7EMLjd9+SI/65oQJCXG0wDtQ+wjwE+1eWAebpVIemMRrRie4j5nLOpxdY5m1KE8kBkRD7UBgb88OWeGX1fMjl4IINFKjo3gX5DF5eVQIhS4pvL1rfp/H+lum21VAVDdKsewnWQnPOyQn1Nq0uhq8UJKmicME7bphH2/Weqy4xI3ehs3lKUKSSpiA2pRwWyeteQNEUbT3seu47c2HN/JkqU9PzdfTSEA+CUNSgLbiNVTebMPEoHp4EtYFzWhgvt6Rrig+M0dyeRIBHn7l/Ljy8qCAlbjBizYTeXecG2r6295Mo/LT9nHZXvBzd03ak7NRvHw788oTvwvitMRSyep+vvhl+dHskCUcwl85iR/wQPPTqgNpwql6xlalNX0uSDUhyKujVyFj6wID5VhNG3JecpgDA4CNMPvbge3cX5HHx0spiqCmflQWtRvnNA03k1LUEbMQQUW5xUi5K/QRA2Jv9FTLzkMyC3KUToTf4cXcJxV8ukvEzxeTMcdelHRONpSvvqJ/R7MVPL+A3nffMDcPLa0pgYSSUIqH/H2DNtjgSecfRi/D77qswLPdSU1maNThCNOAk6lLq2jIysxX4JO4x8P3ciMJNSOg1I1PdAPdHU8wtJsif6hzAsZpQjJtCOPxjSwvgt1+/Wbqe7LUhHWB3NrRpnAewnLg4DFvfOg+dA0Na73kYx6gH2vYkRA1SodBs3jZBgT9IMOJPvl0uM9u008aSWZRMQsAJ/MAD2xVGrlGIgzh2M9IgvqIsD5799jy4w6xg6LHHzWGRdhClT7RH83fHr8C7A4PeXmoBy/FQjc9VSbEXYo4E5mbiXnKhZVetXsjF9xyVRCgRvkziitzvQAluR0ocI+VYXpIHv7q7EOoqImP+RVrWRmtEw4GN8txnP/4CDvUMQZyyf84MuIV4QvtHFuOzlXisoIVD/LYIv46oH2FBFFl7CVncjxPpQfIGf4qHcwuHQrCmYib8Ykk+rJo/K1UIx9j+J8AARfgzgg8/uo4AAAAASUVORK5CYII=\" />\n\t\t\t\t\t</div>\n\t\t\t\t\t<h4 class=\"form-successful__title\">B\u1EA1n \u0111\xE3 \u0111\u0103ng k\xFD t\u01B0 v\u1EA5n th\xE0nh c\xF4ng</h4>\n\t\t\t\t\t<p class=\"form-successful__text\">C\u1EA3m \u01A1n b\u1EA1n, ch\xFAng t\xF4i \u0111\xE3 nh\u1EADn \u0111\u01B0\u1EE3c th\xF4ng tin c\u1EE7a b\u1EA1n<br>Ch\xFAng t\xF4i s\u1EBD s\u1EDBm li\xEAn l\u1EA1c v\u1EDBi b\u1EA1n<p/>\n\t\t\t\t</div>\n\t\t\t";
            } else {
                html = "\n\t\t\t\t<div class=\"form-successful\">\n\t\t\t\t\t<div class=\"form-successful__icon\">\n\t\t\t\t\t\t<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QTMxQkVENEU0OTM0MTFFOUIwOTdDRTQxQjNBOTU4OUQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QTMxQkVENEY0OTM0MTFFOUIwOTdDRTQxQjNBOTU4OUQiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpBMzFCRUQ0QzQ5MzQxMUU5QjA5N0NFNDFCM0E5NTg5RCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpBMzFCRUQ0RDQ5MzQxMUU5QjA5N0NFNDFCM0E5NTg5RCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PnjSU30AAAm4SURBVHja5FsLjFxVGf7PuTO7M7Pv7i6kLbFP7fsBrfSFrYAtlKD4TLStL4yJISLB+k4NMWLUqInGJyQqRokBo01MRBS0RGOEhvBIFau2BcqWtkjdzu7M7J3Offj995w7Mzs7s3NfC9t4kq+7nZ05937n/8//f/9/zwjXden/aaT4n7O7b0h6XkNjIbAeuBxYDswDBoGcvrYNlIBR4DTwT+Ap4EngmP67lTjhBEl2AkuBm4DdwAagI8QcN+qfTPII8DvgIPB3oJwEecEuHdPCKW21a4F9wM6EF9IB/gzcCzwInAEqr5aF+7U1Pw2smKFtJ4EdGieBrwH3Ay9FnSzK4D24Ffg5cM8Mkm0crwG+DfxKe1LPK0F4GPisdrPrX6Vguw34PfAlvQgzRngV8D3gQAzvUMMWRPEz4q3Aj4ErZ4LwG4CfAu+MfZsWyEo3KdLX6K3FWUEkRZgnvUvn03ijIkgMWmRcf57ECqTgCzIJF18M/BB4SxJR+irgO4kEJkd42dq4cZTE1jGSpzvJGk2TewqputOJO/tc4Ps6ZT0Q1cJM8puJReEJSXLLOIm1RaJzaaJem4wbRlkNqGwbfzDpbwGbohAeAu7UaimmvCHPdcVghYwdY2oP89414d7LJkium4DATMS1SSu9rwILwhBmmXg78PbEtBJIyjflieZYijAPHbCMXdjPg5DNZRkg7AQaLFLu0FI3EGEO8/sTuTQTKMKVVxdJvr4wVQ1zpJ5XJmNnXv3uJGVo+qAWJ20J9wFfaLY6kciy1fpsktedRxmhU1H9cHXk3lAgsRJRu2AkZWXSW3LudIT5/29ttjKRB/amvAqBasEFj1jL6J1BALsaVs666n3JkF4H3FzPUzYJVJ9LTPLDWmJhmeSVBeWqbs2o3Heo9h74p4WgttgkiXRFRSNJGXpbfQCTDYGKS7zXJROVgbRLcjdybl2gYpIdKUEDfQZ1d8kaaUepArl9jMQSM0nXZu3/Ns1vEmH+/f2JrSusJDcVSK6cUC6rLZrpFNSRFnT0uEmjeXsyad7LQxbJa84rshYlRZoDWKaR8HIdzuNbl3PqJbjxbeOelf3InIIFixMO3XbnKdrziZO0Z//z9AyIZ7NCkXaVpeVy5Ob1ReXayRDmomcL351sCFaZ2FNzFIb7GjvyJObVAhUT6s4ZdOjRAv3o4H/p6aMTdOixAh18aIxynXJyYZHDVuAABmtzSkuANM/w7nrCqbp+UsxAheCzqkRyQ3GKZDSkoJEzFXL0axLvP3GyTJMap64iLRaUlSpz5NRUFm3sArp9wgt0dzHeGkIrizlIL6yokHsb0xBz4T1ryNpHRDMutvqj3FhQFdW4jFt987gMWOtPsz5kd3HqYDYTkI8sIBaX1U031LouTNuVrUs5YNuVa+Gy7Nr9WDxOU3DxBGQnf3qTT/iK2FMhjUgQlZsQqFJOTS/X6wvcdw8s7FuVfw72I1dL0Vx/Y9HECtOrsMiUKtrHGxtkXRkYI+cKr56VV8Ma81GOVpr7H+/d3h5jEuFLBlAPt9LPPG8WsnQbaudFpor+8Tgv9O9sfqxp4G4cpMTqkrJMEwJMznY4UuO9UlbXamhOynP16bS4mFdRVk65reVpsNHvEx6Ik3PFsKVcuctWVmlVJcKnmXCmQ1QXgQnbTpvSEmGcmwZyTVG5dvTR5X86FzXn4lZIbh73dPB0ZH2X9ghnhE5Lgvrh4rbdRlFxwBqwVRHCuXkismt3yhC9rSZpCGRXwpXZupKaBqrGoNUJ63brSJ3LSBCWsLDb/v65O/Ja09vPZLhtr9VKKci6zBeOrKlz7na96mb7VefnWB7JXnXZ3i5DWThI0c8EQVRsLKq0Z0aysuMTLoVu2XDKWIeLL4Mrl4NdmYnlspJWLc14ll62pJP6PJd2gy/yUEWVm9122y3ULO77rnw+dKBCQS8348JpJ7De9XaB6dDH3jdEA70peseuXri5S4GfyTuqomJx4z7fQc5felXkDs676BM+HXyNODc6qt0KvRtW3Jtl17PwN+6YTzYqp/yYHfzzfs7vUQHMfS5L7kgabuMEfYKR9136aGD5iMgs16J82ziuivyQTTeuhV86Z9NfUSk9O1Kmjo6Qj1uEqrV5seWWMdXAD+7aL/iEnwoeqCwSfKEeJ3TgSKcE5QsO3frFEdp583Hau/8k/eNEuZqmQsUQr/GHGLLUrPW5248nfMJPULvjBPxX7BdxRYEkX6QYroLx6mFE5QceydPBh/NULDl0+EiJvnvvy14gc8NamSuzPsvLEjRgBbEyX+Gwf8snSJ2jaP1W6GO5BDXqG8dqaSKsTkG+7ek2VIXESRFTDA+kWmvpAP1usaakgqch2tXNZ4EnfcL8AOrBlm+t6ECxHXXusB1JxLOMLBRt2r29hz66b4i2XN5F+24aoFv2DnptHxFFR+jGvbENAfSysrrP1p5yiINWqk54/BL4OG+1qVoW1lhVgp7lRnl08c55mDuWX759Lv1n1PJEB7vyeNGpSaAo/bNh5Oat42SfTSvXTjdlzc+Q3frL8DGhx5opHHEpigNut3DOuxC9Se5llYqLwGVTL1zbvOBSoRSRbP1242Y/d0hXl2qvTR7HSR3RcOovxW59z5RWS8b19KtYpHvFMVst7LpcRLAbW5YbzZWb5eaUQ8a10E+XWl5Tv2H8hFsUjUWDrffxqWp97MC6GVtp1yznOyeJ3lL1PhMd3NGF+hOI2C67tlu9CEfZ+/0s1FglseL6CqmjQd4kLnKt/UgfyXMp3adK/FYTGK6KPGfwz2lAOvUrykc1jlGLstDRm3svsFkdPMF0j+fIPpJVB1FmK2HeG6bQpzyrm/hfpE4d2TRNHXwO+DzwUNX30rq14ooZ8MWENoiriU5+DsePfZ+rf6FV4f8n4AfAR6oNdnnRHTPmQ6m/ntIBaFUTkTrl9ge6OAcfPz7gR+YghHmMAJ8B/n2RkWUJ+SngmaY9njYffpzUA+UXLxKyRVJnU1p6ZpCs+lvgFlLnlGc7WTbOfTSNog4qI3jzf6gx4s2i8TLwYa2opi1zgxLmFePjfO/REXw2jSP6vu6jAF8RCCsUHwU+ANw9S8iyZNwDPBy02RRFGT8LfBJ4c6DW0MyMY1oNsk74W5gPRi0FWJD/htSpgQNanb0So6S1/nXahUfDThDnSx6urqy+ri/OZ5XfS3FPEjQfrIl/ppsU3I4yI4vQBL7G4w+uV/hQDJ+WeRepM1+LYsz3om7L/EIHyqJWgLFGkt8vqmhw0fFHoAtYQ+rsMp8wWEjqHGe3XhipA01Zk8kDL2hZeBh4Wr9mURJfFNDjfwIMANO4H4e/F9CbAAAAAElFTkSuQmCC\" />\n\t\t\t\t\t</div>\n\t\t\t\t\t<h4 class=\"form-successful__title\">B\u1EA1n \u0111\xE3 \u0111\u0103ng k\xFD kh\xF4ng th\xE0nh c\xF4ng</h4>\n\t\t\t\t\t<a href=\"#\" class=\"btn btn-secondary back-form-reg-advice\">Quay l\u1EA1i form \u0111\u0103ng k\xFD</a\n\t\t\t\t</div>\n\t\t\t";
            }

            setTimeout(function () {
                $.magnificPopup.open({
                    items: {
                        src: html, // can be a HTML string, jQuery object, or CSS selector
                        type: 'inline'
                    },
                    removalDelay: 500,
                    showCloseBtn: false,
                    closeOnBgClick: true,
                    closeBtnInside: true,
                    callbacks: {
                        beforeOpen: function beforeOpen() {
                            this.st.mainClass = 'mfp-zoom-in';
                        }
                    }
                });
            }, 500);
        });
    }

    /**
     * handlingAjax
     */
    function checkFormValidate() {
        $('.characterbox-max').each(function () {
            var self = $(this),
                input = $('.characterbox__input', self);

            input.on('change', function () {
                if (self.find('.characterbox__input:checked').length > 3) {
                    this.checked = false;
                }
            });
        });
    }

    /**
     * rangerBox
     */
    function rangerBox() {
        $('.rangerbox').each(function () {
            var self = $(this),
                handle = $('.rangerbox-handle', self),
                mins = parseInt(self.attr('data-min')),
                maxs = parseInt(self.attr('data-max')),
                values = parseInt(self.attr('data-value')),
                unit = self.attr('data-unit') ? self.attr('data-unit') : '';

            self.slider({
                range: "min",
                min: mins,
                max: maxs,
                value: values,
                create: function create() {
                    handle.text($(this).slider("value") + unit);
                },
                slide: function slide(event, ui) {
                    handle.text(ui.value + unit);
                }
            });
        });
    }

    /**
     * thugon
     */
    function thugonJs() {
        $('.table-sosanh-content thead').on('click', function () {
            $(this).next().toggle();
        });
    }

    /**
     * footerItemJs
     */
    function footerItemJs() {
        $('.footer__itemtitle').on('click', function () {
            $(this).next().slideToggle();
        });
    }

    /**
     * backtoTop
     */
    function backtoTop() {
        $('.footer__backtotop').on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700);
        });
    }

    /**
     * toolsosanhAcc
     */
    function toolsosanhAcc() {
        $('.tool-sosanh-acc').each(function () {
            var self = $(this),
                item = $('.tool-sosanh-acc__item', self),
                title = $('.tool-sosanh-acc__title', item);

            title.on('click', function () {
                $(this).next().slideToggle();
                $(this).toggleClass('tool-sosanh-acc__title--active');
            });
        });
    }

    /**
     * scrollCategoryMin
     */
    function scrollCategoryMin() {
        var fix = function fix() {
            var scroll = $(window).scrollTop(),
                wh = $(window).outerHeight() / 2;

            if (scroll >= wh) {
                $('.category-min').addClass('category-min--show');
            } else {
                $('.category-min').removeClass('category-min--show');
            }
        };
        fix();
        $(window).on('scroll', fix);
    }

    /**
     * Call function
     */
    heroSelect();
    selecJs();
    fixHiehgtJs();
    popupJs();
    languageJs();
    handlingAjax();
    checkFormValidate();
    rangerBox();
    thugonJs();
    footerItemJs();
    backtoTop();
    toolsosanhAcc();

    $(window).on('load', function () {
        headerPage();
        footerScroll();
        swiperSlides();
        slideYearProcess();
        heroSlide();
        gridJs();
        countTos();
        tabJs();
        accordionJs();
        numberTextLines();
        hoverNewEffect();
        menuBoxMin();
        scrollCategoryMin();
    });

})(jQuery);
