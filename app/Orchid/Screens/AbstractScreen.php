<?php

namespace App\Orchid\Screens;

use Orchid\Screen\Screen;
use App\Orchid\Layout\Link;

abstract class AbstractScreen extends Screen
{
    /**
     * {@inheritdoc}
     */
    public function handle(...$parameters)
    {
        if ($this->request->method() === 'GET' && $this->request->route('method')) {
            abort(404);
        }

        return parent::handle(...$parameters);
    }

    public function addSwitchLocaleLink(): Link
    {
        /* @noinspection PhpIncompatibleReturnTypeInspection */
        return Link::view('platform.container.partials.locales-link');
    }

    public function addLink($name, $route = null): Link
    {
        return tap(Link::name($name), function (Link $link) use ($route) {
            if ($route) {
                $link->link(route($route));
            }
        });
    }
}
