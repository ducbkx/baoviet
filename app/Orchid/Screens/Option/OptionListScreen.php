<?php


namespace App\Orchid\Screens\Option;

use Orchid\Screen\Layout;
use Orchid\Screen\Screen;
use Illuminate\Http\Request;
use Orchid\Platform\Models\User;
use Orchid\Support\Facades\Alert;

class OptionListScreen extends Screen
{
    public function __construct()
    {
        parent::__construct();

        $this->name = __('Options');
    }

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function commandBar(): array
    {
        return [
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function layout(): array
    {
        return [
        ];
    }
}