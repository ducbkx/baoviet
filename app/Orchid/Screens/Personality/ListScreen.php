<?php


namespace App\Orchid\Screens\Personality;

use App\Model\Personality;
use App\Model\User;
use App\Orchid\Screens\AbstractScreen;

class ListScreen extends AbstractScreen
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->name = __('Personalities');
    }

    public function query(): array
    {
        $this->authorize(Personality::PERMISSION_VIEW);

        $items = Personality::withTranslation()
            ->filters()
            ->defaultSort('id', 'desc')
            ->paginate();

        return [
            'items' => $items,
        ];
    }

    public function commandBar(): array
    {
        /* @var $user User */
        $user = $this->request->user();

        return [
            $this->addLink(__('Thêm mới'), 'platform.personalities.create')
                ->canSee($user->can(Personality::PERMISSION_TOUCH))
                ->icon('icon-plus'),
        ];
    }

    public function layout(): array
    {
        return [
            Layouts\ListTable::class,
        ];
    }
}