<?php


namespace App\Orchid\Screens\Personality\Layouts;

use App\Model\Personality;
use Orchid\Screen\TD;
use Orchid\Screen\Layouts\Table;

class ListTable extends Table
{
    /**
     * @var string
     */
    public $data = 'items';

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            TD::set('id', __('ID'))
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->filter('numeric')
                ->sort()
                ->link('platform.personalities.edit', ['id']),

            TD::set('name', __('Tên'))
                ->render(function ($item) {
                    return $item->name;
                }),

            TD::set('description', __('Mô tả'))
                ->render(function ($item) {
                    return $item->description;
                }),

            TD::set('created_at', __('Ngày tạo'))
                ->filter('date')
                ->align(TD::ALIGN_RIGHT)
                ->sort()
                ->render(function (Personality $item) {
                    return $item->created_at->toDateString();
                }),
        ];
    }
}