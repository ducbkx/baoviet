<?php


namespace App\Orchid\Screens\Counselor\Layouts;

use App\Model\Counselor;
use App\Model\Personality;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use App\Orchid\Layout\TwoColumnsLayout;

class EditLayout extends TwoColumnsLayout
{
    /**
     * {@inheritdoc}
     */
    public function mainFields(): array
    {
        return [
            Input::make('counselor.first_name')
                ->type('text')
                ->maxlength(255)
                ->required()
                ->title(__('Tên')),

            Input::make('counselor.last_name')
                ->type('text')
                ->maxlength(255)
                ->required()
                ->title(__('Họ')),

            Input::make('counselor.display_name')
                ->type('text')
                ->maxlength(255)
                ->required()
                ->title(__('Tên hiển thị')),

            Input::make('counselor.year_of_birth')
                ->type('text')
                ->mask('9999')
                ->required()
                ->title(__('Năm sinh')),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function sidebarFields(): array
    {
        return [

            Picture::make('counselor.avatar')
                ->width(500)
                ->height(300)
                ->title(__('Ảnh')),

            Select::make('counselor.gender')
                ->options(Counselor::getGender())
                ->title(__('Giới tính')),

            Select::make('personality.')
                ->options(function () {
                    $options = Personality::getAllPersonalities();
                    return $options;
                })
                ->multiple()
                ->title('Tính cách'),

            Select::make('counselor.rate_value')
                ->options(['1' => '1', '2' =>'2', '3' => '3', '4' => '4', '5' => '5'])
                ->title(__('Đánh giá')),
        ];
    }
}
