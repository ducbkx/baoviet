<?php


namespace App\Orchid\Screens\Counselor\Layouts;

use App\Model\Counselor;
use Orchid\Screen\TD;
use Orchid\Screen\Layouts\Table;

class ListTable extends Table
{
    /**
     * @var string
     */
    public $data = 'items';

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            TD::set('id', __('ID'))
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->filter('numeric')
                ->sort()
                ->link('platform.counselors.edit', ['id']),

            TD::set('avatar', __('Ảnh'))
                ->render(function ($item) {
                    return '<img src="' . $item->avatar .'" alt="" height="50" width="50">';
                }),

            TD::set('title', __('Tên hiển thị'))
                ->render(function ($item) {
                    return $item->display_name;
                }),

            TD::set('created_at', __('Ngày tạo'))
                ->filter('date')
                ->align(TD::ALIGN_RIGHT)
                ->sort()
                ->render(function (Counselor $item) {
                    return $item->created_at->toDateString();
                }),
        ];
    }
}