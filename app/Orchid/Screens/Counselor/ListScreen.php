<?php


namespace App\Orchid\Screens\Counselor;

use App\Model\Counselor;
use App\Model\User;
use App\Orchid\Screens\AbstractScreen;

class ListScreen extends AbstractScreen
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->name = __('Nhân viên tư vấn');
    }

    public function query(): array
    {
        $this->authorize(Counselor::PERMISSION_VIEW);

        $items = Counselor::withTranslation()
            ->filters()
            ->defaultSort('id', 'desc')
            ->paginate();

        return [
            'items' => $items,
        ];
    }

    public function commandBar(): array
    {
        /* @var $user User */
        $user = $this->request->user();

        return [
            $this->addLink(__('Thêm mới'), 'platform.counselors.create')
                ->canSee($user->can(Counselor::PERMISSION_TOUCH))
                ->icon('icon-plus'),
        ];
    }

    public function layout(): array
    {
        return [
            Layouts\ListTable::class,
        ];
    }
}