<?php


namespace App\Orchid\Screens\Counselor;

use App\Model\Counselor;
use App\Model\Counselor_personality;
use App\Model\Personality;
use App\Model\User;
use App\Orchid\Requests\StoreCounselorRequest;
use App\Orchid\Screens\AbstractScreen;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

class EditScreen extends AbstractScreen
{
    /**
     * Indicator the model's exists or not.
     *
     * @var bool
     */
    protected $exist = false;

    /**
     * Query data.
     *
     * @param Request $request
     * @param Counselor $counselor
     * @return array
     *
     * @throws \Throwable
     */
    public function query(Request $request, Counselor $counselor): array
    {
        $this->authorize(Counselor::PERMISSION_TOUCH);
        $this->exist = $counselor->exists;
        $personality = Personality::getPersonalities($counselor->id);
        return ['counselor' => $counselor, 'personality' => $personality];
    }

    /**
     * {@inheritdoc}
     */
    public function commandBar(): array
    {
        /* @var $user User */
        $user = $this->request->user();

        return [
            $this->addLink(__('Tạo'))
                ->icon('icon-check')
                ->method('save')
                ->canSee(!$this->exist),

            $this->addLink(__('Xóa'))
                ->icon('icon-trash')
                ->method('destroy')
                ->canSee($this->exist && $user->can(Counselor::PERMISSION_DELETE)),

            $this->addLink(__('Lưu'))
                ->icon('icon-check')
                ->method('save')
                ->canSee($this->exist),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function layout(): array
    {
        return [
            Layouts\EditLayout::class,
        ];
    }

    /**
     * Handle the "save" command action.
     *
     * @param Request $request
     * @param Counselor $counselor
     * @return RedirectResponse
     *
     * @throws \Throwable
     */
    public function save(StoreCounselorRequest $request, Counselor $counselor): RedirectResponse
    {
        $validated = $request->validated()['counselor'];

        $counselor->fill([
            'first_name' => sanitize_text_field($validated['first_name'] ?? ''),
            'last_name' => sanitize_text_field($validated['last_name'] ?? ''),
            'display_name' => sanitize_text_field($validated['display_name'] ?? ''),
            'year_of_birth' => (int) $validated['year_of_birth'],
            'avatar' => sanitize_text_field($validated['avatar'] ?? ''),
            'gender' => sanitize_text_field($validated['gender'] ?? ''),
            'rate_value' => (int) $validated['rate_value'],
        ]);

        $personality = $request->validated()['personality'] ?? [];

        if (is_array($personality)){
            $counselor->personality()->sync($personality);
        }

        $counselor->saveOrFail();
        alert()->success(__('Lưu thành công.'));

        return redirect()->route('platform.counselors.edit', $counselor);
    }

    /**
     * Handle the "destroy" command action.
     *
     * @param Counselor $counselor
     * @return RedirectResponse
     *
     * @throws \Exception
     */
    public function destroy(Counselor $counselor): RedirectResponse
    {
        $this->checkPermission(Counselor::PERMISSION_DELETE);

        $counselor->delete();
        alert()->success(__('Xóa thành công.'));

        return redirect()->route('platform.counselors');
    }
}
