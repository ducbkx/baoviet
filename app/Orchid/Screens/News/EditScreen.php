<?php

namespace App\Orchid\Screens\News;

use App\Model\Categoriable;
use App\Model\Category;
use App\Model\News;
use App\Orchid\Requests\StoreNewRequest;
use App\Orchid\Screens\AbstractScreen;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Orchid\Press\Models\Term;

class EditScreen extends AbstractScreen
{
    /**
     * Indicator the model's exists or not.
     *
     * @var bool
     */
    protected $exist = false;

    /**
     * Query data.
     *
     * @param News $news
     * @return array
     */
    public function query(Request $request, News $news): array
    {
        $this->authorize(News::PERMISSION_TOUCH);

        $this->exist = $news->exists;

        $category = Category::getCategory($news->id);

        return [
            'news' => $news,
            'category' => $category
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function commandBar(): array
    {
        $user = $this->request->user();

        return [
            $this->addLink(__('Tạo'))
                ->icon('icon-check')
                ->method('save')
                ->canSee(!$this->exist),

            $this->addLink(__('Xóa'))
                ->icon('icon-trash')
                ->method('destroy')
                ->canSee($this->exist && $user->can(News::PERMISSION_DELETE)),

            $this->addLink(__('Lưu'))
                ->icon('icon-check')
                ->method('save')
                ->canSee($this->exist),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function layout(): array
    {
        return [
            Layouts\EditLayout::class,
        ];
    }

    public function save(StoreNewRequest $request, News $news): RedirectResponse
    {
        $validated = $request->validated()['news'] ?? [];

        $news->fill([
            'title' => sanitize_text_field($validated['title'] ?? ''),
            'slug' => sanitize_text_field($validated['slug'] ?? ''),
            'excerpt' => sanitize_textarea_field($validated['excerpt'] ?? ''),
            'content' => wp_kses_post($validated['content'] ?? ''),
        ]);
        $news->saveOrFail();

        $category = $request->validated()['category'] ?? [];
            $news->categories()->sync($category);

        alert()->success(__('Lưu thành công.'));

        return redirect()->route('platform.news.edit', $news);
    }

    public function destroy(News $news): RedirectResponse
    {
        $this->checkPermission(News::PERMISSION_DELETE);

        $news->delete();
        alert()->success(__('Xóa thành công.'));

        return redirect()->route('platform.news');
    }
}
