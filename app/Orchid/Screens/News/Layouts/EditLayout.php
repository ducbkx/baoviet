<?php

namespace App\Orchid\Screens\News\Layouts;

use App\Model\Category;
use App\Model\Translations\CategoryTranslation;
use Orchid\Screen\Field;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Code;
use Orchid\Screen\Fields\DateTimer;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Map;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Fields\Quill;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\SimpleMDE;
use Orchid\Screen\Fields\Tags;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Fields\TinyMCE;
use Orchid\Screen\Fields\Upload;
use Orchid\Screen\Fields\UTM;
use App\Orchid\Layout\TwoColumnsLayout;

class EditLayout extends TwoColumnsLayout
{
    /**
     * {@inheritdoc}
     */
    public function mainFields(): array
    {
        return [

            Input::make('news.title')
                ->type('text')
                ->max(255)
                ->required()
                ->title('Tiêu đề')
                ->help('Nhập vào tiêu đề'),


            TinyMCE::make('news.content')
                ->required()
                ->title('Nội dung'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function sidebarFields(): array
    {
        return [
            // Field:

            Input::make('news.slug')
                ->type('text')
                ->max(255)
                ->title('Đường dẫn tĩnh')
                ->placeholder(__('Nhập url duy nhất')),

            TextArea::make('news.excerpt')
                ->required()
                ->title('Đoạn trích'),

            Select::make('category.')
                ->options(function () {
                    $options = Category::getAllCategories();
                    return $options;
                })
                ->multiple()
                ->title('Danh mục')
                ->help('Lựa chọn danh mục'),

            Tags::make('tags')
                ->title('Tags')
                ->help('Keywords'),

        ];
    }
}
