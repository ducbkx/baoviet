<?php

namespace App\Orchid\Screens\News\Layouts;

use App\Model\News;
use Orchid\Screen\TD;
use Orchid\Screen\Layouts\Table;
use App\Orchid\Screens\News\ListScreen;

class ListTable extends Table
{
    /**
     * @var string
     */
    public $data = ListScreen::DATA_NEWS_KEY;

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            TD::set('id', __('ID'))
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->link('platform.news.edit', ['id'])
                ->filter('numeric')
                ->sort(),

            TD::set('title', __('Tiêu đề'))
                ->render(function ($new) {
                    return $new->title;
                })
                ->sort(),

            TD::set('slug', __('Đường dẫn tĩnh'))
                ->loadModalAsync('oneAsyncModal', 'saveSlug', 'id', 'slug')
                ->render(function ($new) {
                    return $new->slug;
                })
                ->sort(),

            TD::set('created_at', __('Ngày tạo'))
                ->filter('date')
                ->align(TD::ALIGN_RIGHT)
                ->sort()
                ->render(function (News $item) {
                    return $item->created_at->toDateString();
                }),
        ];
    }
}
