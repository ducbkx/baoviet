<?php

namespace App\Orchid\Screens\News;

use App\Model\News;
use App\Orchid\Screens\AbstractScreen;

class ListScreen extends AbstractScreen
{
    /* Constants */
    public const DATA_NEWS_KEY = 'news';

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->name = __('Tin tức');
    }

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        $news = News::withTranslation()
            ->filters()
            ->defaultSort('id', 'desc')
            ->paginate();

        return [
            self::DATA_NEWS_KEY => $news,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function commandBar(): array
    {
        $user = $this->request->user();

        return [
            $this->addLink(__('Thêm mới'), 'platform.news.create')
                ->canSee($user->can(News::PERMISSION_TOUCH))
                ->icon('icon-plus'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function layout(): array
    {
        return [
            Layouts\ListTable::class,
        ];
    }
}
