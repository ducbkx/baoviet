<?php


namespace App\Orchid\Screens\Category;

use App\Model\Category;
use App\Model\User;
use App\Orchid\Screens\AbstractScreen;

class ListScreen extends AbstractScreen
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->name = __('Danh mục');
    }

    public function query(): array
    {
        $this->authorize(Category::PERMISSION_VIEW);

        $items = Category::withTranslation()
            ->filters()
            ->defaultSort('id', 'desc')
            ->paginate();

        return [
            'items' => $items,
        ];
    }

    public function commandBar(): array
    {
        /* @var $user User */
        $user = $this->request->user();

        return [
            $this->addLink(__('Thêm mới'), 'platform.categories.create')
                ->canSee($user->can(Category::PERMISSION_TOUCH))
                ->icon('icon-plus'),
        ];
    }

    public function layout(): array
    {
        return [
            Layouts\ListTable::class,
        ];
    }
}
