<?php


namespace App\Orchid\Screens\Category\Layouts;

use App\Model\Category;
use Orchid\Screen\TD;
use Orchid\Screen\Layouts\Table;

class ListTable extends Table
{
    /**
     * @var string
     */
    public $data = 'items';

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            TD::set('id', __('ID'))
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->filter('numeric')
                ->sort()
                ->link('platform.categories.edit', ['id']),

            TD::set('name', __('Tên'))
                ->render(function ($item) {
                    return $item->name;
                }),

            TD::set('description', __('Mô tả'))
                ->render(function ($item) {
                    return $item->description;
                }),

            TD::set('created_at', __('Ngày tạo'))
                ->filter('date')
                ->align(TD::ALIGN_RIGHT)
                ->sort()
                ->render(function (Category $item) {
                    return $item->created_at->toDateString();
                }),
        ];
    }
}