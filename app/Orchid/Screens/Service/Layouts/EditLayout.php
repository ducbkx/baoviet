<?php


namespace App\Orchid\Screens\Service\Layouts;

use App\Model\Service;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TinyMCE;
use App\Orchid\Layout\TwoColumnsLayout;

class EditLayout extends TwoColumnsLayout
{
    /**
     * {@inheritdoc}
     */
    public function mainFields(): array
    {
        return [
            Input::make('service.title')
                ->type('text')
                ->maxlength(255)
                ->required()
                ->title(__('Tiêu đề'))
                ->help('Nhập vào tiêu đề'),

            Input::make('service.order')
                ->type('text')
                ->required()
                ->title(__('Thứ tự')),

            TinyMCE::make('service.content')
                ->required()
                ->title(__('Nội dung')),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function sidebarFields(): array
    {
        return [
            Input::make('service.slug')
                ->type('text')
                ->max(255)
                ->title(__('Đường dẫn tĩnh'))
                ->placeholder(__('Nhập url duy nhất')),

            Select::make('service.status')
                ->options(Service::getStatus())
                ->title(__('Trạng thái')),
        ];
    }
}