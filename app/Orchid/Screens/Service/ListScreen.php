<?php


namespace App\Orchid\Screens\Service;

use App\Model\Service;
use App\Model\User;
use App\Orchid\Screens\AbstractScreen;

class ListScreen extends AbstractScreen
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->name = __('Services');
    }

    public function query(): array
    {
        $this->authorize(Service::PERMISSION_VIEW);

        $items = Service::withTranslation()
            ->filters()
            ->defaultSort('order', 'desc')
            ->paginate();

        return [
            'items' => $items,
        ];
    }

    public function commandBar(): array
    {
        /* @var $user User */
        $user = $this->request->user();

        return [
            $this->addLink(__('Thêm mới'), 'platform.services.create')
                ->canSee($user->can(Service::PERMISSION_TOUCH))
                ->icon('icon-plus'),
        ];
    }

    public function layout(): array
    {
        return [
            Layouts\ListTable::class,
        ];
    }
}
