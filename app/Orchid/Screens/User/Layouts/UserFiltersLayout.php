<?php

namespace App\Orchid\Screens\User\Layouts;

use Orchid\Platform\Filters\Filter;
use Orchid\Screen\Layouts\Selection;
use App\Orchid\Screens\User\Filters\RoleFilter;

class UserFiltersLayout extends Selection
{
    /**
     * @return Filter[]
     */
    public function filters(): array
    {
        return [
            RoleFilter::class,
        ];
    }
}
