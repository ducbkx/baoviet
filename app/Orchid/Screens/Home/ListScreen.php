<?php


namespace App\Orchid\Screens\Home;

use App\Model\Page;
use App\Model\User;
use App\Orchid\Screens\AbstractScreen;

class ListScreen extends AbstractScreen
{
    public function __construct()
    {
        parent::__construct();

        $this->name = __('Trang chủ');
    }

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        $page = Page::firstOrCreate(['type' => 'home']);

        return [
            'page' => $page,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function commandBar(): array
    {
        return [
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function layout(): array
    {
        return [
        ];
    }
}
