<?php


namespace App\Orchid\Screens\Product;

use App\Model\ProductCategory;
use App\Model\Product;
use App\Orchid\Requests\StoreProductRequest;
use App\Orchid\Screens\AbstractScreen;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Orchid\Press\Models\Term;

class EditScreen extends AbstractScreen
{
    /**
     * Indicator the model's exists or not.
     *
     * @var bool
     */
    protected $exist = false;

    /**
     * Query data.
     *
     * @param Product $product
     * @return array
     */
    public function query(Request $request, Product $product): array
    {
        $this->authorize(Product::PERMISSION_TOUCH);

        $this->exist = $product->exists;

        $category = ProductCategory::getCategory($product->id);

        return [
            'product' => $product,
            'category' => $category
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function commandBar(): array
    {
        $user = $this->request->user();

        return [
            $this->addLink(__('Tạo'))
                ->icon('icon-check')
                ->method('save')
                ->canSee(!$this->exist),

            $this->addLink(__('Xóa'))
                ->icon('icon-trash')
                ->method('destroy')
                ->canSee($this->exist && $user->can(Product::PERMISSION_DELETE)),

            $this->addLink(__('Lưu'))
                ->icon('icon-check')
                ->method('save')
                ->canSee($this->exist),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function layout(): array
    {
        return [
            Layouts\EditLayout::class,
        ];
    }

    public function save(StoreProductRequest $request, Product $product): RedirectResponse
    {
        $validated = $request->validated()['product'] ?? [];

        $product->fill([
            'title' => sanitize_text_field($validated['title'] ?? ''),
            'slug' => sanitize_text_field($validated['slug'] ?? ''),
            'excerpt' => sanitize_textarea_field($validated['excerpt'] ?? ''),
            'content' => wp_kses_post($validated['content'] ?? ''),
            'image' => sanitize_text_field($validated['image'] ?? ''),
            'is_future' => (int)$validated['is_future']
        ]);
        $product->saveOrFail();

        $category = $request->validated()['category'] ?? [];
        $product->productCategories()->sync($category);

        alert()->success(__('Lưu thành công.'));

        return redirect()->route('platform.products.edit', $product);
    }

    public function destroy(Product $product): RedirectResponse
    {
        $this->checkPermission(Product::PERMISSION_DELETE);

        $product->delete();
        alert()->success(__('Xóa thành công.'));

        return redirect()->route('platform.products');
    }
}
