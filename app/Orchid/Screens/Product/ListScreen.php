<?php


namespace App\Orchid\Screens\Product;

use App\Model\Product;
use App\Orchid\Screens\AbstractScreen;

class ListScreen extends AbstractScreen
{
    /* Constants */
    public const DATA_NEWS_KEY = 'products';

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->name = __('Sản phẩm');
    }

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        $product = Product::withTranslation()
            ->filters()
            ->defaultSort('id', 'desc')
            ->paginate();

        return [
            self::DATA_NEWS_KEY => $product,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function commandBar(): array
    {
        $user = $this->request->user();

        return [
            $this->addLink(__('Thêm mới'), 'platform.products.create')
                ->canSee($user->can(Product::PERMISSION_TOUCH))
                ->icon('icon-plus'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function layout(): array
    {
        return [
            Layouts\ListTable::class,
        ];
    }
}
