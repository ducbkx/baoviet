<?php


namespace App\Orchid\Screens\Product\Layouts;

use App\Model\Product;
use Orchid\Screen\TD;
use Orchid\Screen\Layouts\Table;
use App\Orchid\Screens\Product\ListScreen;

class ListTable extends Table
{
    /**
     * @var string
     */
    public $data = ListScreen::DATA_NEWS_KEY;

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            TD::set('id', __('ID'))
                ->align(TD::ALIGN_CENTER)
                ->width('100px')
                ->link('platform.products.edit', ['id'])
                ->filter('numeric')
                ->sort(),

            TD::set('title', __('Tiêu đề'))
                ->render(function ($product) {
                    return $product->title;
                })
                ->sort(),

            TD::set('slug', __('Đường dẫn tĩnh'))
                ->loadModalAsync('oneAsyncModal', 'saveSlug', 'id', 'slug')
                ->render(function ($product) {
                    return $product->slug;
                })
                ->sort(),

            TD::set('created_at', __('Ngày tạo'))
                ->filter('date')
                ->align(TD::ALIGN_RIGHT)
                ->sort()
                ->render(function (Product $item) {
                    return $item->created_at->toDateString();
                }),
        ];
    }
}
