<?php


namespace App\Orchid\Screens\Product\Layouts;

use App\Model\ProductCategory;
use Orchid\Screen\Fields\CheckBox;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\Tags;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Fields\TinyMCE;
use App\Orchid\Layout\TwoColumnsLayout;

class EditLayout extends TwoColumnsLayout
{
    /**
     * {@inheritdoc}
     */
    public function mainFields(): array
    {
        return [

            Select::make('category.')
                ->options(function () {
                    $options = ProductCategory::getAllCategories();
                    return $options;
                })
                ->title('Danh mục')
                ->help('Lựa chọn danh mục'),

            Input::make('product.title')
                ->type('text')
                ->max(255)
                ->required()
                ->title('Tiêu đề')
                ->help('Nhập vào tiêu đề'),


            TinyMCE::make('product.content')
                ->required()
                ->title('Nội dung'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function sidebarFields(): array
    {
        return [
            // Field:

            Picture::make('product.image')
                ->width(500)
                ->height(300)
                ->title(__('Ảnh')),

            Input::make('product.slug')
                ->type('text')
                ->max(255)
                ->title('Đường dẫn tĩnh')
                ->placeholder(__('Nhập url duy nhất')),

            TextArea::make('product.excerpt')
                ->required()
                ->title('Đoạn trích'),

            CheckBox::make('product.is_future')
                ->value(1)
                ->title(__('Sản phẩm phổ biến'))
                ->help(__('Đánh dấu sản phẩm phổ biến')),

            Tags::make('tags')
                ->title('Tags')
                ->help('Keywords'),

        ];
    }
}
