<?php


namespace App\Orchid\Screens\ProductCategory;

use App\Model\ProductCategory;
use App\Model\User;
use App\Orchid\Screens\AbstractScreen;

class ListScreen extends AbstractScreen
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->name = __('Danh mục sản phẩm');
    }

    public function query(): array
    {
        $this->authorize(ProductCategory::PERMISSION_VIEW);

        $items = ProductCategory::withTranslation()
            ->filters()
            ->defaultSort('id', 'desc')
            ->paginate();

        return [
            'items' => $items,
        ];
    }

    public function commandBar(): array
    {
        /* @var $user User */
        $user = $this->request->user();

        return [
            $this->addLink(__('Thêm mới'), 'platform.products.categories.create')
                ->canSee($user->can(ProductCategory::PERMISSION_TOUCH))
                ->icon('icon-plus'),
        ];
    }

    public function layout(): array
    {
        return [
            Layouts\ListTable::class,
        ];
    }
}
