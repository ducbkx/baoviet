<?php

namespace App\Orchid\Screens\Page\Layouts;

use App\Model\Page;
use Orchid\Screen\TD;
use Orchid\Screen\Layouts\Table;

class ListTable extends Table
{
    /**
     * @var string
     */
    public $data = 'items';

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            TD::set('id', __('ID'))
              ->align(TD::ALIGN_CENTER)
              ->width('100px')
              ->filter('numeric')
              ->sort()
              ->link('platform.pages.edit', ['id']),

            TD::set('title', __('Tiêu đề'))
              ->render(function ($item) {
                  return $item->title;
              }),

            TD::set('created_at', __('Ngày tạo'))
              ->filter('date')
              ->align(TD::ALIGN_RIGHT)
              ->sort()
              ->render(function (Page $item) {
                  return $item->created_at->toDateString();
              }),
        ];
    }
}
