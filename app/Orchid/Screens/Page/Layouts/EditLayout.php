<?php

namespace App\Orchid\Screens\Page\Layouts;

use App\Model\Page;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\TinyMCE;
use App\Orchid\Layout\TwoColumnsLayout;

class EditLayout extends TwoColumnsLayout
{
    /**
     * {@inheritdoc}
     */
    public function mainFields(): array
    {
        return [
            Input::make('page.title')
                 ->type('text')
                 ->maxlength(255)
                 ->required()
                 ->title(__('Tiêu đề'))
                 ->help('Nhập vào tiêu đề'),

            TinyMCE::make('page.content')
                   ->required()
                   ->title(__('Nội dung')),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function sidebarFields(): array
    {
        return [
            Input::make('page.slug')
                 ->type('text')
                 ->max(255)
                 ->title(__('Đường dẫn tĩnh'))
                 ->placeholder(__('Nhập url duy nhất')),

            Select::make('page.status')
                  ->options(Page::getStatus())
                  ->title(__('Trạng thái')),
        ];
    }
}
