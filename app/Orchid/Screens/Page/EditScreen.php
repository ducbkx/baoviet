<?php

namespace App\Orchid\Screens\Page;

use App\Model\Page;
use App\Model\User;
use App\Orchid\Screens\AbstractScreen;
use App\Orchid\Requests\StorePageRequest;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

class EditScreen extends AbstractScreen
{
    /**
     * Indicator the model's exists or not.
     *
     * @var bool
     */
    protected $exist = false;

    /**
     * Query data.
     *
     * @param  Request  $request
     * @param  Page  $page
     * @return array
     *
     * @throws \Throwable
     */
    public function query(Request $request, Page $page): array
    {
        $this->authorize(Page::PERMISSION_TOUCH);

        $this->exist = $page->exists;
        return ['page' => $page];
    }

    /**
     * {@inheritdoc}
     */
    public function commandBar(): array
    {
        /* @var $user User */
        $user = $this->request->user();

        return [
            $this->addLink(__('Tạo'))
                 ->icon('icon-check')
                 ->method('save')
                 ->canSee(!$this->exist),

            $this->addLink(__('Xóa'))
                 ->icon('icon-trash')
                 ->method('destroy')
                 ->canSee($this->exist && $user->can(Page::PERMISSION_DELETE)),

            $this->addLink(__('Lưu'))
                 ->icon('icon-check')
                 ->method('save')
                 ->canSee($this->exist),

            $this->addSwitchLocaleLink(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function layout(): array
    {
        return [
            Layouts\EditLayout::class,
        ];
    }

    /**
     * Handle the "save" command action.
     *
     * @param  Request  $request
     * @param  Page  $page
     * @return RedirectResponse
     *
     * @throws \Throwable
     */
    public function save(StorePageRequest $request, Page $page): RedirectResponse
    {
        $validated = $request->validated()['page'] ?? [];

        $page->fill([
            'title' => sanitize_text_field($validated['title'] ?? ''),
            'content' => wp_kses_post($validated['content'] ?? ''),
            'slug' => sanitize_text_field($validated['slug'] ?? ''),
            'status' => sanitize_text_field($validated['status'] ?? '')
        ]);

        $page->saveOrFail();
        alert()->success(__('Lưu thành công.'));

        return redirect()->route('platform.pages.edit', $page);
    }

    /**
     * Handle the "destroy" command action.
     *
     * @param  Page  $page
     * @return RedirectResponse
     *
     * @throws \Exception
     */
    public function destroy(Page $page): RedirectResponse
    {
        $this->checkPermission(Page::PERMISSION_DELETE);

        $page->delete();
        alert()->success(__('Xóa thành công.'));

        return redirect()->route('platform.pages');
    }
}
