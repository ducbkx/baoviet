<?php

namespace App\Orchid\Screens\Page;

use App\Model\Page;
use App\Model\User;
use App\Orchid\Screens\AbstractScreen;

class ListScreen extends AbstractScreen
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->name = __('Trang');
    }

    public function query(): array
    {
        $this->authorize(Page::PERMISSION_VIEW);

        $items = Page::withTranslation()
             ->filters()
             ->defaultSort('id', 'desc')
             ->paginate();

        return [
            'items' => $items,
        ];
    }

    public function commandBar(): array
    {
        /* @var $user User */
        $user = $this->request->user();

        return [
            $this->addLink(__('Thêm mới'), 'platform.pages.create')
                 ->canSee($user->can(Page::PERMISSION_TOUCH))
                 ->icon('icon-plus'),
        ];
    }

    public function layout(): array
    {
        return [
            Layouts\ListTable::class,
        ];
    }
}
