<?php

declare(strict_types=1);

namespace App\Orchid;

use Orchid\Platform\Dashboard;
use Orchid\Platform\ItemPermission;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Auth\Access\Authorizable;
use App\Orchid\Composers\MainMenuComposer;
use App\Orchid\Composers\SystemMenuComposer;

class PlatformProvider extends ServiceProvider
{
    /**
     * Boot the application events.
     *
     * @param Dashboard $dashboard
     *
     * @throws \Throwable
     */
    public function boot(Dashboard $dashboard): void
    {
        View::composer('platform::layouts.dashboard', MainMenuComposer::class);
        View::composer('platform::container.systems.index', SystemMenuComposer::class);

        $this->registerPermissionsOnGate(
            $this->app->make(Gate::class)
        );

        $dashboard
            ->registerPermissions($this->registerPermissionsForPage())
            ->registerPermissions($this->registerPermissionsForNews())
            ->registerPermissions($this->registerPermissionsForServices())
            ->registerPermissions($this->registerPermissionsForCategories())
            ->registerPermissions($this->registerPermissionsForCounselors())
            ->registerPermissions($this->registerPermissionsForPersonalities())
            ->registerPermissions($this->registerPermissionsForProductCategories())
            ->registerPermissions($this->registerPermissionsForProducts())
            ->registerPermissions($this->registerPermissionsSystems());

        $dashboard->registerGlobalSearch([
            //...Models
        ]);
    }

    /**
     * Register the permission check method on the gate.
     *
     * @param Gate $gate
     * @return void
     */
    public function registerPermissionsOnGate(Gate $gate): void
    {
        $gate->before(function (Authorizable $user, string $ability): bool {
            if (method_exists($user, 'hasAccess')) {
                return $user->hasAccess($ability);
            }

            return true;
        });
    }

    /**
     * @return ItemPermission
     */
    protected function registerPermissionsSystems(): ItemPermission
    {
        return ItemPermission::group(__('Systems'))
            ->addPermission('platform.systems.menu', __('Menu'))
            ->addPermission('platform.systems.roles', __('Roles'))
            ->addPermission('platform.systems.users', __('Users'));
    }

    /**
     * @return ItemPermission
     */
    protected function registerPermissionsForPage(): ItemPermission
    {
        return ItemPermission::group(__('Pages'))
            ->addPermission('platform.pages.view', __('View pages'))
            ->addPermission('platform.pages.touch', __('Create or update pages'))
            ->addPermission('platform.pages.delete', __('Trash or delete pages'));
    }

    /**
     * @return ItemPermission
     */
    protected function registerPermissionsForNews(): ItemPermission
    {
        return ItemPermission::group(__('News'))
            ->addPermission('platform.news.view', __('View News'))
            ->addPermission('platform.news.touch', __('Create or update news'))
            ->addPermission('platform.news.delete', __('Trash or delete news'));
    }

    /**
     * @return ItemPermission
     */
    protected function registerPermissionsForServices(): ItemPermission
    {
        return ItemPermission::group(__('Services'))
            ->addPermission('platform.services.view', __('View Services'))
            ->addPermission('platform.services.touch', __('Create or update services'))
            ->addPermission('platform.services.delete', __('Trash or delete services'));
    }

    /**
     * @return ItemPermission
     */
    protected function registerPermissionsForCategories(): ItemPermission
    {
        return ItemPermission::group(__('Categories'))
            ->addPermission('platform.categories.view', __('View Categories'))
            ->addPermission('platform.categories.touch', __('Create or update categories'))
            ->addPermission('platform.categories.delete', __('Trash or delete categories'));
    }

    /**
     * @return ItemPermission
     */
    protected function registerPermissionsForCounselors(): ItemPermission
    {
        return ItemPermission::group(__('Counselors'))
            ->addPermission('platform.counselors.view', __('View Counselors'))
            ->addPermission('platform.counselors.touch', __('Create or update counselors'))
            ->addPermission('platform.counselors.delete', __('Trash or delete counselors'));
    }

    /**
     * @return ItemPermission
     */
    protected function registerPermissionsForPersonalities(): ItemPermission
    {
        return ItemPermission::group(__('Personalities'))
            ->addPermission('platform.personalities.view', __('View Personalities'))
            ->addPermission('platform.personalities.touch', __('Create or update personalities'))
            ->addPermission('platform.personalities.delete', __('Trash or delete personalities'));
    }

    protected function registerPermissionsForProductCategories(): ItemPermission
    {
        return ItemPermission::group(__('Product Categories'))
            ->addPermission('platform.products_categories.view', __('View Product Categories'))
            ->addPermission('platform.products_categories.touch', __('Create or update product categories'))
            ->addPermission('platform.products_categories.delete', __('Trash or delete product categories'));
    }

    protected function registerPermissionsForProducts(): ItemPermission
    {
        return ItemPermission::group(__('Products'))
            ->addPermission('platform.products.view', __('View Products'))
            ->addPermission('platform.products.touch', __('Create or update products'))
            ->addPermission('platform.products.delete', __('Trash or delete products'));
    }
}
