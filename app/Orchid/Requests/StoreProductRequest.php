<?php


namespace App\Orchid\Requests;

use App\Model\News;
use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return $this->user()->can(News::PERMISSION_TOUCH);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'product.title'   => 'required|min:3|max:255',
            'product.slug'    => 'nullable|alpha_dash|min:3',
            'product.excerpt' => 'required|min:3',
            'product.content' => 'required|min:3',
            'product.image' => 'max:2048',
            'product.is_future' => '',
            'category' => 'required'
        ];
    }
}
