<?php


namespace App\Orchid\Requests;

use App\Model\Category;
use Illuminate\Foundation\Http\FormRequest;

class StoreCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return $this->user()->can(Category::PERMISSION_TOUCH);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'category.name'   => 'required|min:3|max:255',
            'category.description' => 'required|min:3',
            'category.slug'    => 'nullable|alpha_dash|min:3',
        ];
    }
}