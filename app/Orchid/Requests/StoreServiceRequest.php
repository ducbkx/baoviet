<?php


namespace App\Orchid\Requests;

use App\Model\Service;
use Illuminate\Foundation\Http\FormRequest;

class StoreServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return $this->user()->can(Service::PERMISSION_TOUCH);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'service.title' => 'required|min:3|max:255',
            'service.order' => 'required|numeric',
            'service.content' => 'required|min:3',
            'service.slug' => 'nullable|alpha_dash|min:3',
            'service.status' => 'required',
        ];
    }
}