<?php

namespace App\Orchid\Requests;

use App\Model\Page;
use Illuminate\Foundation\Http\FormRequest;

class StorePageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return $this->user()->can(Page::PERMISSION_TOUCH);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'page.title'   => 'required|min:3|max:255',
            'page.content' => 'required|min:3',
            'page.slug'    => 'nullable|alpha_dash|min:3',
            'page.status'  => 'required',
        ];
    }
}
