<?php


namespace App\Orchid\Requests;

use App\Model\Counselor;
use Illuminate\Foundation\Http\FormRequest;
use phpDocumentor\Reflection\Types\Nullable;

class StoreCounselorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return $this->user()->can(Counselor::PERMISSION_TOUCH);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'counselor.first_name'   => 'required|min:3|max:255',
            'counselor.last_name'   => 'required|min:3|max:255',
            'counselor.display_name'   => 'required|min:3|max:255',
            'counselor.year_of_birth'   => 'numeric',
            'counselor.avatar' => 'max:2048',
            'counselor.gender'   => 'required',
            'counselor.rate_value'   => 'required',
            'personality' => 'nullable|array'
        ];
    }
}