<?php


namespace App\Orchid\Requests;

use App\Model\Personality;
use Illuminate\Foundation\Http\FormRequest;

class StorePersonalityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return $this->user()->can(Personality::PERMISSION_TOUCH);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'personality.name'   => 'required|min:3|max:255',
            'personality.description' => 'required|min:3',
            'personality.slug'    => 'nullable|alpha_dash|min:3',
        ];
    }
}
