<?php


namespace App\Orchid\Requests;

use App\Model\News;
use Illuminate\Foundation\Http\FormRequest;

class StoreNewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return $this->user()->can(News::PERMISSION_TOUCH);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'news.title'   => 'required|min:3|max:255',
            'news.slug'    => 'nullable|alpha_dash|min:3',
            'news.excerpt' => 'required|min:3',
            'news.content' => 'required|min:3',
            'category' => 'nullable|array'
        ];
    }
}
