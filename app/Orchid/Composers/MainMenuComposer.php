<?php

declare(strict_types=1);

namespace App\Orchid\Composers;

use App\Model\Category;
use App\Model\Counselor;
use App\Model\News;
use App\Model\Personality;
use App\Model\Product;
use App\Model\ProductCategory;
use App\Model\Service;
use App\Model\Page;
use Illuminate\Support\Facades\Auth;
use Orchid\Platform\Menu;
use Orchid\Platform\ItemMenu;
use Orchid\Platform\Dashboard;

class MainMenuComposer
{
    /**
     * @var Dashboard
     */
    private $dashboard;

    /**
     * MenuComposer constructor.
     *
     * @param  Dashboard  $dashboard
     */
    public function __construct(Dashboard $dashboard)
    {
        $this->dashboard = $dashboard;
    }

    /**
     * Registering the main menu items.
     */
    public function compose()
    {
        $user = Auth::user();
        // Profile
        $this->dashboard->menu
            ->add(
                Menu::PROFILE,
                ItemMenu::label('Example 3')
                    ->icon('icon-microphone')
            );

        // Main
        $this->dashboard->menu
            ->add(
                Menu::MAIN,
                ItemMenu::label(__('Trang'))
                    ->icon('icon-book-open')
                    ->route('platform.pages')
                    ->groupName(__('Trang'))
                    ->show($user->can(Page::PERMISSION_VIEW))
            )
            ->add(
                Menu::MAIN,
                ItemMenu::label(__('Trang chủ'))
                    ->icon('icon-home')
                    ->route('platform.home')
            )
            ->add(
                Menu::MAIN,
                ItemMenu::label(__('Tin tức'))
                    ->icon('icon-pin')
                    ->route('platform.news')
                    ->groupName(__('Tin tức'))
                    ->show($user->can(News::PERMISSION_VIEW))
            )
            ->add(
                Menu::MAIN,
                ItemMenu::label(__('Danh mục'))
                    ->icon('icon-list')
                    ->route('platform.categories')
                    ->show($user->can(Category::PERMISSION_VIEW))
            )
            ->add(
                Menu::MAIN,
                ItemMenu::label(__('Dịch vụ'))
                    ->icon('icon-notebook')
                    ->route('platform.services')
                    ->groupName(__('Dịch vụ'))
                    ->show($user->can(Service::PERMISSION_VIEW))
            )
            ->add(
                Menu::MAIN,
                ItemMenu::label(__('Nhân viên tư vấn'))
                    ->icon('icon-user')
                    ->route('platform.counselors')
                    ->groupName(__('Nhân viên tư vấn'))
                    ->show($user->can(Counselor::PERMISSION_VIEW))
            )
            ->add(
                Menu::MAIN,
                ItemMenu::label(__('Tính cách'))
                    ->icon('icon-list')
                    ->route('platform.personalities')
                    ->show($user->can(Personality::PERMISSION_VIEW))

            )
            ->add(
                Menu::MAIN,
                ItemMenu::label(__('Danh mục sản phẩm'))
                    ->icon('icon-list')
                    ->route('platform.products.categories')
                    ->groupName(__('Sản phẩm'))
                    ->show($user->can(ProductCategory::PERMISSION_VIEW))
            )

            ->add(
                Menu::MAIN,
                ItemMenu::label(__('Sản phẩm'))
                    ->icon('icon-notebook')
                    ->route('platform.products')
                    ->show($user->can(Product::PERMISSION_VIEW))
            );

    }
}
