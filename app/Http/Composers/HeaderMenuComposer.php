<?php

namespace App\Http\Composers;

use Lavary\Menu\Builder;
use Lavary\Menu\Facade as Menu;

class HeaderMenuComposer
{
    public function compose(): void
    {
        Menu::make('navbar', function (Builder $menu) {
            $menu->add('Home');
        });
    }
}
