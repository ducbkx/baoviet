<?php

namespace App\Http\Controllers;

use App\Model\Page;
use App\Model\Services\PageService;

class PageController extends Controller
{
    public function home()
    {
        $page = PageService::getHomePage(
            $this->getCurrentLocale()
        );

        return view('page', ['page' => $page]);
    }

    public function show(Page $page)
    {
        return view('page', ['page' => $page]);
    }
}
