<?php

namespace App\Model;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Orchid\Platform\Traits\FilterTrait;

/**
 * App\Model\Category
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\News[] $news
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\Translations\CategoryTranslation[] $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Category listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Category notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Category orWhereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Category orWhereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Category orderByTranslation($key, $sortmethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Category translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Category translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Category whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Category whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Category withTranslation()
 * @mixin \Eloquent
 */
class Category extends Model
{
    use FilterTrait,
        Helpers\Translatable;

    /* Constants */
    public const PERMISSION_VIEW = 'platform.categories.view';
    public const PERMISSION_TOUCH = 'platform.categories.touch';
    public const PERMISSION_DELETE = 'platform.categories.delete';

    public static $check = null;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * Definition the attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [
        'name', 'slug', 'description',
    ];

    public function news(): MorphToMany
    {
        return $this->morphedByMany(News::class, 'categoriable');
    }

    public static function getCategory($newsId)
    {
        $categoryId = self::join('categoriables', 'categories.id', '=', 'categoriables.category_id')
            ->where('categoriables.categoriable_id', $newsId)
            ->where('categoriables.categoriable_type', News::class)
            ->pluck('categories.id');
        $categories = self::$check ? : self::whereIn('id', $categoryId)->get();
        return $categories->mapWithKeys(function ($item) {
            return [$item->id => $item->name];
        })->toArray();
    }

    /**
     * Select all categories, except current.
     *
     * @return array
     */
    public static function getAllCategories()
    {
        $categories = self::get();

        return $categories->mapWithKeys(function ($item) {
            return [$item->id => $item->name];
        })->toArray();
    }

    public static function boot()
    {
        parent::boot();

        static::deleting(function (self $category) {
            $category->news()
                ->detach();
        });
    }
}
