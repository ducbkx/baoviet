<?php

namespace App\Model\Services;

use App\Model\Page;

class PageService
{
    public static function getById($id)
    {
        return Page::findOrFail($id);
    }

    public static function getBySlug($slug, $locale = null)
    {
        return Page::whereTranslation('slug', $slug, $locale)->firstOrFail();
    }

    public static function getHomePage($locale = null)
    {
        return Page::where('type', Page::TYPE_HOME)
                   ->translatedIn($locale)
                   ->firstOrFail();
    }
}
