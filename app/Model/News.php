<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Orchid\Platform\Traits\FilterTrait;

/**
 * App\Model\News
 *
 * @property int $id
 * @property int $author_id
 * @property string $status
 * @property int $order
 * @property string|null $published_date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Model\User $author
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\Category[] $categories
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\Translations\NewsTranslation[] $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\News defaultSort($column, $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\News filters()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\News filtersApply($filters = array())
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\News findSimilarSlugs($attribute, $config, $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\News listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\News newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\News newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\News notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\News orWhereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\News orWhereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\News orderByTranslation($key, $sortmethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\News query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\News translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\News translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\News whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\News whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\News whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\News whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\News wherePublishedDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\News whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\News whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\News whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\News whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\News withTranslation()
 * @mixin \Eloquent
 */
class News extends Model
{
    use FilterTrait,
        Helpers\Translatable;

    /* Constants */
    public const PERMISSION_VIEW = 'platform.news.view';
    public const PERMISSION_TOUCH = 'platform.news.touch';
    public const PERMISSION_DELETE = 'platform.news.delete';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'news';

    /**
     * Definition the attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [
        'title', 'slug', 'excerpt', 'content', 'content_filtered',
    ];

    public function author(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function categories(): MorphToMany
    {
        return $this->morphToMany(Category::class, 'categoriable');
    }

}
