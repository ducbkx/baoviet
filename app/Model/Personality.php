<?php

namespace App\Model;

use App\Model\Helpers\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Orchid\Platform\Traits\FilterTrait;

class Personality extends Model
{
    use Helpers\Translatable;
    use FilterTrait;

    /* Constants */
    public const PERMISSION_VIEW = 'platform.personalities.view';
    public const PERMISSION_TOUCH = 'platform.personalities.touch';
    public const PERMISSION_DELETE = 'platform.personalities.delete';

    public static $check = null;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'personalities';

    /**
     * Definition the attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [
        'name', 'slug', 'description',
    ];

    public function counselor()
    {
        return $this->belongsToMany(Counselor::class);
    }

    public static function getPersonalities($counselorId)
    {
        $personalityId = self::join('counselor_personality', 'personalities.id', '=', 'counselor_personality.personality_id')
            ->where('counselor_personality.counselor_id', $counselorId)
            ->pluck('personalities.id');
        $personalities = self::$check ?  : self::whereIn('id', $personalityId)->get();
        return $personalities->mapWithKeys(function ($item) {
            return [$item->id => $item->name];
        })->toArray();
    }


    /**
     * Select all categories, except current.
     *
     * @return array
     */
    public static function getAllPersonalities()
    {
        $personalities = self::get();
        return $personalities->mapWithKeys(function ($item) {
            return [$item->id => $item->name];
        })->toArray();
    }
}
