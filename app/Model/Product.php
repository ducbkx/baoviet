<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Orchid\Platform\Traits\FilterTrait;

class Product extends Model
{
    use FilterTrait,
        Helpers\Translatable;

    /* Constants */
    public const PERMISSION_VIEW = 'platform.products.view';
    public const PERMISSION_TOUCH = 'platform.products.touch';
    public const PERMISSION_DELETE = 'platform.products.delete';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'image', 'is_future'
    ];

    /**
     * Definition the attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [
        'title', 'slug', 'excerpt', 'content', 'content_filtered',
    ];

    public function author(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function productCategories(): MorphToMany
    {
        return $this->morphToMany(ProductCategory::class, 'product_categoriable');
    }

}
