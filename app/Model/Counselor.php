<?php

namespace App\Model;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Orchid\Platform\Traits\FilterTrait;

class Counselor extends Model

{
    use FilterTrait,
        Helpers\Translatable;
    /* Constants */
    public const GENDER_MAN = 'men';
    public const GENDER_WOMAN = 'women';
    public const GENDER_ANOTHER = 'another';
    public const PERMISSION_VIEW = 'platform.counselors.view';
    public const PERMISSION_TOUCH = 'platform.counselors.touch';
    public const PERMISSION_DELETE = 'platform.counselors.delete';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'counselors';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'gender', 'year_of_birth','job_title','rate_value','avatar'
    ];

    /**
     * //
     *
     * @var array
     */
    protected $allowedSorts = [
        'id', 'created_at', 'updated_at',
    ];

    /**
     * //
     *
     * @var array
     */
    protected $allowedFilters = [
        'id',
    ];

    /**
     * Definition the attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [
        'first_name', 'last_name', 'display_name',
    ];

    /**
     * Returns the page status.
     *
     * @return array
     */
    public static function getGender(): array
    {
        return [
            self::GENDER_MAN => __('Nam'),
            self::GENDER_WOMAN => __('Nữ'),
            self::GENDER_ANOTHER => __('Giới tính khác'),
        ];
    }

    public function personality()
    {
        return $this->belongsToMany(Personality::class);
    }
}
