<?php

namespace App\Model;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Orchid\Platform\Traits\FilterTrait;

/**
 * App\Model\Page
 *
 * @property int $id
 * @property string $type
 * @property string $status
 * @property int $order
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\Translations\PageTranslation[] $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Page defaultSort($column, $direction = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Page filters()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Page filtersApply($filters = array())
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Page listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Page newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Page newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Page notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Page orWhereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Page orWhereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Page orderByTranslation($key, $sortmethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Page query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Page translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Page translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Page whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Page whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Page whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Page whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Page whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Page whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Page whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Page withTranslation()
 * @mixin \Eloquent
 */
class Page extends Model
{
    use FilterTrait,
        Helpers\Translatable;

    /* Constants */
    public const TYPE_PAGE = 'page';
    public const TYPE_HOME = 'page';
    public const STATUS_PENDING = 'pending';
    public const STATUS_PUBLISH = 'publish';
    public const PERMISSION_VIEW = 'platform.pages.view';
    public const PERMISSION_TOUCH = 'platform.pages.touch';
    public const PERMISSION_DELETE = 'platform.pages.delete';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status', 'order', 'type'
    ];

    /**
     * //
     *
     * @var array
     */
    protected $allowedSorts = [
        'id', 'created_at', 'updated_at',
    ];

    /**
     * //
     *
     * @var array
     */
    protected $allowedFilters = [
        'id',
    ];

    /**
     * Definition the attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [
        'title', 'slug', 'content', 'content_filtered',
    ];

    /**
     * {@inheritdoc}
     */
    public static function boot()
    {
        parent::boot();

        static::saving(function (self $page) {
            $page->type = $page->type ?: self::TYPE_PAGE;
        });
    }

    /**
     * Returns the page status.
     *
     * @return array
     */
    public static function getStatus(): array
    {
        return [
            self::STATUS_PUBLISH => __('Publish'),
            self::STATUS_PENDING => __('Pending'),
        ];
    }
}
