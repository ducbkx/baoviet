<?php

namespace App\Model\Helpers;

trait Sluggable
{
    use \Cviebrock\EloquentSluggable\Sluggable;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source'    => 'title',
                'maxLength' => 100,
                'separator' => '-',
                'unique'    => true,
            ],
        ];
    }
}
