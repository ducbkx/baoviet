<?php

namespace App\Model\Helpers;

use Arcanedev\Localization\Facades\Localization;

trait Translatable
{
    use \Dimsav\Translatable\Translatable {
        getLocales as parentGetLocales;
    }

    /**
     * {@inheritdoc}
     */
    protected function getLocales()
    {
        if (!config('translatable.locales')) {
            return Localization::getSupportedLocalesKeys();
        }

        return $this->parentGetLocales();
    }
}
