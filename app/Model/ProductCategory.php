<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Orchid\Platform\Traits\FilterTrait;

class ProductCategory extends Model
{
    use FilterTrait,
        Helpers\Translatable;

    /* Constants */
    public const PERMISSION_VIEW = 'platform.products_categories.view';
    public const PERMISSION_TOUCH = 'platform.products_categories.touch';
    public const PERMISSION_DELETE = 'platform.products_categories.delete';

    public static $check = null;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product_categories';

    /**
     * Definition the attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [
        'name', 'slug', 'description',
    ];

    public function product(): MorphToMany
    {
        return $this->morphedByMany(Product::class, 'product_categoriable');
    }

    public static function getCategory($productId)
    {
        $categoryId = self::join('product_categoriables', 'product_categories.id', '=',
            'product_categoriables.product_category_id')
            ->where('product_categoriables.product_categoriable_id', $productId)
            ->where('product_categoriables.product_categoriable_type', Product::class)
            ->pluck('product_categories.id')
            ->first();

        $categories = self::$check ?: self::where('id', $categoryId)->get();
        return $categories->mapWithKeys(function ($item) {
            return [$item->id => $item->name];
        })->toArray();

    }

    /**
     * Select all categories, except current.
     *
     * @return array
     */
    public static function getAllCategories()
    {
        $categories = self::get();

        return $categories->mapWithKeys(function ($item) {
            return [$item->id => $item->name];
        })->toArray();
    }

    public static function boot()
    {
        parent::boot();

        static::deleting(function (self $category) {
            $category->product()
                ->detach();
        });
    }
}
