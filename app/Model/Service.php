<?php

namespace App\Model;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Orchid\Platform\Traits\FilterTrait;

class Service extends Model
{
    use FilterTrait,
        Helpers\Translatable;

    /* Constants */
    public const TYPE_SERVICE = 'service';
    public const TYPE_HOME = 'service';
    public const STATUS_PENDING = 'pending';
    public const STATUS_PUBLISH = 'publish';
    public const PERMISSION_VIEW = 'platform.services.view';
    public const PERMISSION_TOUCH = 'platform.services.touch';
    public const PERMISSION_DELETE = 'platform.services.delete';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'services';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status', 'order'
    ];

    /**
     * //
     *
     * @var array
     */
    protected $allowedSorts = [
        'id', 'created_at', 'updated_at',
    ];

    /**
     * //
     *
     * @var array
     */
    protected $allowedFilters = [
        'id',
    ];

    /**
     * Definition the attributes that are translatable.
     *
     * @var array
     */
    public $translatedAttributes = [
        'title', 'slug', 'content', 'content_filtered',
    ];

    /**
     * Returns the page status.
     *
     * @return array
     */
    public static function getStatus(): array
    {
        return [
            self::STATUS_PUBLISH => __('Publish'),
            self::STATUS_PENDING => __('Pending'),
        ];
    }
}
