<?php

namespace App\Model\Translations;

use App\Model\Helpers\Sluggable;

/**
 * App\Model\Translations\PageTranslation
 *
 * @property int $id
 * @property int $page_id
 * @property string $locale
 * @property string $title
 * @property string $slug
 * @property string $content
 * @property string|null $content_filtered
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Translations\PageTranslation findSimilarSlugs($attribute, $config, $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Translations\PageTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Translations\PageTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Translations\PageTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Translations\PageTranslation whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Translations\PageTranslation whereContentFiltered($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Translations\PageTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Translations\PageTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Translations\PageTranslation wherePageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Translations\PageTranslation whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Translations\PageTranslation whereTitle($value)
 * @mixin \Eloquent
 */
class PageTranslation extends Translation
{
    use Sluggable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pages_translations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'content', 'slug'
    ];
}
