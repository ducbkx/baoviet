<?php

namespace App\Model\Translations;

use App\Model\Helpers\Sluggable;

class CounselorTranslation extends Translation
{
    use Sluggable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'counselors_translations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'display_name',
    ];
}
