<?php

namespace App\Model\Translations;

use App\Model\Helpers\Sluggable;
/**
 * App\Model\Translations\CategoryTranslation
 *
 * @property int $id
 * @property int $category_id
 * @property string $locale
 * @property string $name
 * @property string $slug
 * @property string $description
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Translations\CategoryTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Translations\CategoryTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Translations\CategoryTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Translations\CategoryTranslation whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Translations\CategoryTranslation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Translations\CategoryTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Translations\CategoryTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Translations\CategoryTranslation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Translations\CategoryTranslation whereSlug($value)
 * @mixin \Eloquent
 */
class CategoryTranslation extends Translation
{

    use Sluggable;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories_translations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description','slug'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'name'        => 'string',
        'description' => 'string',
    ];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source'    => 'name',
                'maxLength' => 100,
                'separator' => '-',
                'unique'    => true,
            ],
        ];
    }
}
