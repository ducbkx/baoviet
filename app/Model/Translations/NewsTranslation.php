<?php

namespace App\Model\Translations;

use App\Model\Helpers\Sluggable;

/**
 * App\Model\Translations\NewsTranslation
 *
 * @property int $id
 * @property int $news_id
 * @property string $locale
 * @property string $title
 * @property string $slug
 * @property string $excerpt
 * @property string $content
 * @property string|null $content_filtered
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Translations\NewsTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Translations\NewsTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Translations\NewsTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Translations\NewsTranslation whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Translations\NewsTranslation whereContentFiltered($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Translations\NewsTranslation whereExcerpt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Translations\NewsTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Translations\NewsTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Translations\NewsTranslation whereNewsId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Translations\NewsTranslation whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Translations\NewsTranslation whereTitle($value)
 * @mixin \Eloquent
 */
class NewsTranslation extends Translation
{
    use Sluggable;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'news_translations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'excerpt', 'content',
    ];
}
