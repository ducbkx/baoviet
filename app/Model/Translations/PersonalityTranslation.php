<?php

namespace App\Model\Translations;

use Illuminate\Database\Eloquent\Model;
use App\Model\Helpers\Sluggable;

class PersonalityTranslation extends Translation
{

    use Sluggable;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'personalities_translations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description','slug'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'name'        => 'string',
        'description' => 'string',
    ];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source'    => 'name',
                'maxLength' => 100,
                'separator' => '-',
                'unique'    => true,
            ],
        ];
    }
}
