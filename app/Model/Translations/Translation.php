<?php

namespace App\Model\Translations;

use Illuminate\Database\Eloquent\Model;

abstract class Translation extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
