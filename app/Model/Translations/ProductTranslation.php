<?php


namespace App\Model\Translations;

use App\Model\Helpers\Sluggable;

class ProductTranslation extends Translation
{
    use Sluggable;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products_translations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'excerpt', 'content',
    ];
}
