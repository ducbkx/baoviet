<?php

namespace App;

use Orchid\Platform\Menu;

class BaoViet
{
    /**
     * @var Menu
     */
    public $menu;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->menu = new Menu;
    }

    public function getMenu(): Menu
    {
        return $this->menu;
    }
}
