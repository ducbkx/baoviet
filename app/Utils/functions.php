<?php

require_once __DIR__.DIRECTORY_SEPARATOR.'wp.php';

if (!function_exists('wp_kses_version')) {
    require_once __DIR__.DIRECTORY_SEPARATOR.'kses.php';
}

if (!function_exists('wpautop')) {
    require_once __DIR__.DIRECTORY_SEPARATOR.'formatting.php';
}

/**
 * Returns an URL adapted to $locale or current locale.
 *
 * @param  string  $url
 * @param  string|null  $locale
 *
 * @return string
 */
function localized_url(string $url = '/', string $locale = null)
{
    return localization()->localizeURL($url, $locale);
}
