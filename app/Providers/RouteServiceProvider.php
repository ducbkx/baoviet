<?php

namespace App\Providers;

use App\Model;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Arcanedev\Localization\Facades\Localization;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * The models binding.
     *
     * @see https://laravel.com/docs/5.8/routing#route-model-binding
     *
     * @var array
     */
    protected $models = [
        'pageId' => Model\Page::class,
        'newsId' => Model\News::class,
        'userId' => Model\User::class,
        'serviceId' => Model\Service::class,
        'categoryId' => Model\Category::class,
        'counselorId' => Model\Counselor::class,
        'personalityId' => Model\Personality::class,
        'productCategoryId' => Model\ProductCategory::class,
        'productId' => Model\Product::class,
    ];

    /**
     * Store current locale based on request URI.
     *
     * @var string|null
     */
    protected $locale;

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        // $this->locale = Localization::setLocale();

        $this->bindings();

        parent::boot();
    }

    /**
     * Register models binding.
     *
     * @return void
     */
    protected function bindings(): void
    {
        foreach ($this->models as $model => $class) {
            Route::model($model, $class);
        }

        Route::bind('page', function ($slug, $route) {
            return Model\Page::whereTranslation('slug', $slug)->firstOrFail();
        });
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map(): void
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes(): void
    {
        Route::middleware('web')
             ->namespace($this->namespace)
            // ->prefix($this->locale ?: $this->app->getLocale())
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes(): void
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
