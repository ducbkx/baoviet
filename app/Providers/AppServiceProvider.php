<?php

namespace App\Providers;

use App\Http\Composers\HeaderMenuComposer;
use App\Model;
use App\BaoViet;
use App\Model\Observers;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(BaoViet::class, function () {
            return new BaoViet();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        require app_path('Utils'.DIRECTORY_SEPARATOR.'functions.php');

        View::composer('layouts.main', HeaderMenuComposer::class);

        Model\News::observe(Observers\NewsObserver::class);
        Model\Product::observe(Observers\ProductObserver::class);
    }
}
