<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\PageController;
use App\Http\Controllers\AboutController;

Route::transGet('routes.about', [AboutController::class, 'index']);
Route::transGet('routes.about.show', [AboutController::class, 'show']);

Route::get('/{page}', [PageController::class, 'show']);
Route::get('/', [PageController::class, 'home']);
