<?php

declare(strict_types=1);

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
use DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator as Trail;

// Platform > System > Users
Breadcrumbs::for('platform.systems.users', function (Trail $trail) {
    $trail->parent('platform.systems.index');
    $trail->push(__('Người dùng'), route('platform.systems.users'));
});

// Platform > System > Options
Breadcrumbs::for('platform.systems.options', function (Trail $trail) {
    $trail->parent('platform.systems.index');
    $trail->push(__('Options'), route('platform.systems.options'));
});

// Platform > System > Users > User
Breadcrumbs::for('platform.systems.users.edit', function (Trail $trail, $user) {
    $trail->parent('platform.systems.users');
    $trail->push(__('Sửa'), route('platform.systems.users.edit', $user));
});

// Platform > System > Roles
Breadcrumbs::for('platform.systems.roles', function (Trail $trail) {
    $trail->parent('platform.systems.index');
    $trail->push(__('Roles'), route('platform.systems.roles'));
});

// Platform > System > Roles > Create
Breadcrumbs::for('platform.systems.roles.create', function (Trail $trail) {
    $trail->parent('platform.systems.roles');
    $trail->push(__('Create'), route('platform.systems.roles.create'));
});

// Platform > System > Roles > Role
Breadcrumbs::for('platform.news.edit', function (Trail $trail, $new) {
    $trail->parent('platform.news');
    $trail->push($new->title, route('platform.news.edit', $new));
});

// Platform > Pages
Breadcrumbs::for('platform.pages', function (Trail $trail) {
    $trail->parent('platform.index');
    $trail->push(__('Trang'), route('platform.pages'));
});

// Platform > Pages > Create Page
Breadcrumbs::for('platform.pages.create', function (Trail $trail) {
    $trail->parent('platform.pages');
    $trail->push(__('Thêm trang'), route('platform.news.create'));
});

// Platform > News > {Page Title}
Breadcrumbs::for('platform.pages.edit', function (Trail $trail, $page) {
    $trail->parent('platform.pages');
    $trail->push($page->title, route('platform.news.edit', $page));
});

// Platform > News
Breadcrumbs::for('platform.news', function (Trail $trail) {
    $trail->parent('platform.index');
    $trail->push(__('Tin tức'), route('platform.news'));
});

// Platform > News > Create News
Breadcrumbs::for('platform.news.create', function (Trail $trail) {
    $trail->parent('platform.news');
    $trail->push(__('Thêm tin tức'), route('platform.news.create'));
});

// Platform > Services
Breadcrumbs::for('platform.services', function (Trail $trail) {
    $trail->parent('platform.index');
    $trail->push(__('Dịch vụ'), route('platform.services'));
});

// Platform > Services > Create Service
Breadcrumbs::for('platform.services.create', function (Trail $trail) {
    $trail->parent('platform.services');
    $trail->push(__('Thêm dịch vụ'), route('platform.services.create'));
});

// Platform > Services > {Service Title}
Breadcrumbs::for('platform.services.edit', function (Trail $trail, $service) {
    $trail->parent('platform.services');
    $trail->push($service->title, route('platform.services.edit', $service));
});

// Platform > Categories
Breadcrumbs::for('platform.categories', function (Trail $trail) {
    $trail->parent('platform.index');
    $trail->push(__('Danh mục'), route('platform.categories'));
});

// Platform > Services > Create Category
Breadcrumbs::for('platform.categories.create', function (Trail $trail) {
    $trail->parent('platform.categories');
    $trail->push(__('Thêm danh mục'), route('platform.categories.create'));
});

// Platform > Categories > {Category Name}
Breadcrumbs::for('platform.categories.edit', function (Trail $trail, $category) {
    $trail->parent('platform.categories');
    $trail->push($category->name, route('platform.categories.edit', $category));
});

// Platform > Counselors
Breadcrumbs::for('platform.counselors', function (Trail $trail) {
    $trail->parent('platform.index');
    $trail->push(__('Nhân viên tư vấn'), route('platform.counselors'));
});

// Platform > Services > Create Counselor
Breadcrumbs::for('platform.counselors.create', function (Trail $trail) {
    $trail->parent('platform.counselors');
    $trail->push(__('Thêm nhân viên'), route('platform.counselors.create'));
});

// Platform > Categories > {Counselor Name}
Breadcrumbs::for('platform.counselors.edit', function (Trail $trail, $counselor) {
    $trail->parent('platform.counselors');
    $trail->push($counselor->display_name, route('platform.counselors.edit', $counselor));
});

// Platform > Personalities
Breadcrumbs::for('platform.personalities', function (Trail $trail) {
    $trail->parent('platform.index');
    $trail->push(__('Tính cách'), route('platform.personalities'));
});

// Platform > Services > Create Personality
Breadcrumbs::for('platform.personalities.create', function (Trail $trail) {
    $trail->parent('platform.categories');
    $trail->push(__('Thêm tính cách'), route('platform.categories.create'));
});

// Platform > Personalities > {Personality Name}
Breadcrumbs::for('platform.personalities.edit', function (Trail $trail, $personality) {
    $trail->parent('platform.personalities');
    $trail->push($personality->name, route('platform.personalities.edit', $personality));
});

// Platform > Categories
Breadcrumbs::for('platform.products.categories', function (Trail $trail) {
    $trail->parent('platform.index');
    $trail->push(__('Danh mục sản phẩm'), route('platform.products.categories'));
});

// Platform > Services > Create Category
Breadcrumbs::for('platform.products.categories.create', function (Trail $trail) {
    $trail->parent('platform.products.categories');
    $trail->push(__('Thêm danh mục'), route('platform.products.categories.create'));
});

// Platform > Categories > {Category Name}
Breadcrumbs::for('platform.products.categories.edit', function (Trail $trail, $category) {
    $trail->parent('platform.products.categories');
    $trail->push($category->name, route('platform.products.categories.edit', $category));
});

// Platform > Products
Breadcrumbs::for('platform.products', function (Trail $trail) {
    $trail->parent('platform.index');
    $trail->push(__('Sản phẩm'), route('platform.products'));
});

// Platform > Create Product
Breadcrumbs::for('platform.products.create', function (Trail $trail) {
    $trail->parent('platform.products');
    $trail->push(__('Thêm sản phẩm'), route('platform.products.create'));
});

// Platform > Product> {Product Name}
Breadcrumbs::for('platform.products.edit', function (Trail $trail, $product) {
    $trail->parent('platform.products');
    $trail->push($product->title, route('platform.products.edit', $product));
});
