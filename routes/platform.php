<?php

declare(strict_types=1);

use App\Orchid\Screens\Page;
use App\Orchid\Screens\News;
use App\Orchid\Screens\Service;
use App\Orchid\Screens\Category;
use App\Orchid\Screens\Counselor;
use App\Orchid\Screens\Option;
use App\Orchid\Screens\Home;
use App\Orchid\Screens\ProductCategory;
use App\Orchid\Screens\Product;
use App\Orchid\Screens\EditorScreen;
use App\Orchid\Screens\Personality;
use App\Orchid\Screens\PlatformScreen;
use App\Orchid\Screens\Role\RoleEditScreen;
use App\Orchid\Screens\Role\RoleListScreen;
use App\Orchid\Screens\User\UserEditScreen;
use App\Orchid\Screens\User\UserListScreen;
use Orchid\Press\Http\Controllers\MenuController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the need "dashboard" middleware group. Now create something great!
|
*/

// Main
Route::screen('/main', PlatformScreen::class)->name('platform.main');

// Editor
Route::screen('/editor', EditorScreen::class)->name('platform.gutenberg');

// News
Route::screen('news/{newsId}/edit', News\EditScreen::class)->name('platform.news.edit');
Route::screen('news/create', News\EditScreen::class)->name('platform.news.create');
Route::screen('news', News\ListScreen::class)->name('platform.news');

// Pages
Route::screen('pages/{pageId}/edit', Page\EditScreen::class)->name('platform.pages.edit');
Route::screen('pages/create', Page\EditScreen::class)->name('platform.pages.create');
Route::screen('pages', Page\ListScreen::class)->name('platform.pages');

// Users...
Route::screen('users/{users}/edit', UserEditScreen::class)->name('platform.systems.users.edit');
Route::screen('users', UserListScreen::class)->name('platform.systems.users');

// Options
Route::screen('options', Option\OptionListScreen::class)->name('platform.systems.options');

// Roles...
Route::screen('roles/{roles}/edit', RoleEditScreen::class)->name('platform.systems.roles.edit');
Route::screen('roles/create', RoleEditScreen::class)->name('platform.systems.roles.create');
Route::screen('roles', RoleListScreen::class)->name('platform.systems.roles');

// Sevices
Route::screen('services/{serviceId}/edit', Service\EditScreen::class)->name('platform.services.edit');
Route::screen('services/create', Service\EditScreen::class)->name('platform.services.create');
Route::screen('services', Service\ListScreen::class)->name('platform.services');

// Categories
Route::screen('categories/{categoryId}/edit', Category\EditScreen::class)->name('platform.categories.edit');
Route::screen('categories/create', Category\EditScreen::class)->name('platform.categories.create');
Route::screen('categories', Category\ListScreen::class)->name('platform.categories');

// Counselors
Route::screen('counselors/{counselorId}/edit', Counselor\EditScreen::class)->name('platform.counselors.edit');
Route::screen('counselors/create', Counselor\EditScreen::class)->name('platform.counselors.create');
Route::screen('counselors', Counselor\ListScreen::class)->name('platform.counselors');
//Personalities
Route::screen('personalities/{personalityId}/edit', Personality\EditScreen::class)->name('platform.personalities.edit');
Route::screen('personalities/create', Personality\EditScreen::class)->name('platform.personalities.create');
Route::screen('personalities', Personality\ListScreen::class)->name('platform.personalities');

//Products-Categories
Route::screen('product/categories/{productCategoryId}/edit', ProductCategory\EditScreen::class)->name('platform.products.categories.edit');
Route::screen('product/categories/create', ProductCategory\EditScreen::class)->name('platform.products.categories.create');
Route::screen('product/categories', ProductCategory\ListScreen::class)->name('platform.products.categories');

//Products
Route::screen('products/{productId}/edit', Product\EditScreen::class)->name('platform.products.edit');
Route::screen('products/create', Product\EditScreen::class)->name('platform.products.create');
Route::screen('products', Product\ListScreen::class)->name('platform.products');

// home
Route::screen('home', Home\ListScreen::class)->name('platform.home');

Route::resource('menu', MenuController::class, [
    'only'  => [
        'index', 'show', 'update', 'store', 'destroy',
    ],
    'names' => [
        'index'   => 'platform.systems.menu.index',
        'show'    => 'platform.systems.menu.show',
        'update'  => 'platform.systems.menu.update',
        'store'   => 'platform.systems.menu.store',
        'destroy' => 'platform.systems.menu.destroy',
    ],
]);
